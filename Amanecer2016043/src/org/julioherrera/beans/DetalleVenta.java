package org.julioherrera.beans;

public class DetalleVenta {
    private int codigoDetalleVenta;
    private int cantidad;
    private double precioVenta;
    private double productoIva;
    private int codigoVenta;
    private int codigoProducto;

    public DetalleVenta() {
    }

    public DetalleVenta(int codigoDetalleVenta, int cantidad, double precioVenta, double productoIva, int codigoVenta, int codigoProducto) {
        this.codigoDetalleVenta = codigoDetalleVenta;
        this.cantidad = cantidad;
        this.precioVenta = precioVenta;
        this.productoIva = productoIva;
        this.codigoVenta = codigoVenta;
        this.codigoProducto = codigoProducto;
    }

    public int getCodigoDetalleVenta() {
        return codigoDetalleVenta;
    }

    public void setCodigoDetalleVenta(int codigoDetalleVenta) {
        this.codigoDetalleVenta = codigoDetalleVenta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public double getProductoIva() {
        return productoIva;
    }

    public void setProductoIva(double productoIva) {
        this.productoIva = productoIva;
    }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

}
