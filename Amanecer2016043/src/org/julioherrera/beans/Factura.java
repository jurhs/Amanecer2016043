package org.julioherrera.beans;

import java.util.Date;

public class Factura {
    private int codigoFactura;
    private String numeroFactura;
    private Date fecha;
    private String nitCliente;
    private int cantidad;
    private double precioVenta;
    private double total;
    private int codigoProducto;
    private int codigoCliente;

    public Factura() {
    }

    public Factura(int codigoFactura, String numeroFactura, Date fecha, String nitCliente, int cantidad, double precioVenta, double total, int codigoProducto, int codigoCliente) {
        this.codigoFactura = codigoFactura;
        this.numeroFactura = numeroFactura;
        this.fecha = fecha;
        this.nitCliente = nitCliente;
        this.cantidad = cantidad;
        this.precioVenta = precioVenta;
        this.total = total;
        this.codigoProducto = codigoProducto;
        this.codigoCliente = codigoCliente;
    }

    public int getCodigoFactura() {
        return codigoFactura;
    }

    public void setCodigoFactura(int codigoFactura) {
        this.codigoFactura = codigoFactura;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNitCliente() {
        return nitCliente;
    }

    public void setNitCliente(String nitCliente) {
        this.nitCliente = nitCliente;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

}
