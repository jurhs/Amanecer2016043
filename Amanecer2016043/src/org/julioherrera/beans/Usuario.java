package org.julioherrera.beans;

public class Usuario {
    private int codigoUsuario;
    private String nombresUsuario;
    private String apellidosUsuario;
    private String usuario;
    private String contrasena;

    public Usuario() {
    }

    public Usuario(int codigoUsuario, String nombresUsuario, String apellidosUsuario, String usuario, String contrasena) {
        this.codigoUsuario = codigoUsuario;
        this.nombresUsuario = nombresUsuario;
        this.apellidosUsuario = apellidosUsuario;
        this.usuario = usuario;
        this.contrasena = contrasena;
    }

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getNombresUsuario() {
        return nombresUsuario;
    }

    public void setNombresUsuario(String nombresUsuario) {
        this.nombresUsuario = nombresUsuario;
    }

    public String getApellidosUsuario() {
        return apellidosUsuario;
    }

    public void setApellidosUsuario(String apellidosUsuario) {
        this.apellidosUsuario = apellidosUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
    
    public String toString(){
        return getCodigoUsuario() + " - " + getUsuario() + " - " + getNombresUsuario() + " " + getApellidosUsuario();
    }

}
