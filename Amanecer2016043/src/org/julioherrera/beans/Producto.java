package org.julioherrera.beans;

import java.util.Date;

public class Producto {
    private int codigoProducto;
    private String nombreProducto;
    private int existencia;
    private double precioCosto;
    private double precioVenta;
    private double productoIva;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private int codigoTipoProducto;
    private int codigoProveedor;

    public Producto() {
    }

    public Producto(int codigoProducto, String nombreProducto, int existencia, double precioCosto, double precioVenta, double productoIva, Date fechaCreacion, Date fechaModificacion, int codigoTipoProducto, int codigoProveedor) {
        this.codigoProducto = codigoProducto;
        this.nombreProducto = nombreProducto;
        this.existencia = existencia;
        this.precioCosto = precioCosto;
        this.precioVenta = precioVenta;
        this.productoIva = productoIva;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.codigoTipoProducto = codigoTipoProducto;
        this.codigoProveedor = codigoProveedor;
    }

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public int getExistencia() {
        return existencia;
    }

    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }

    public double getPrecioCosto() {
        return precioCosto;
    }

    public void setPrecioCosto(double precioCosto) {
        this.precioCosto = precioCosto;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public double getProductoIva() {
        return productoIva;
    }

    public void setProductoIva(double productoIva) {
        this.productoIva = productoIva;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public int getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    public void setCodigoTipoProducto(int codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    public int getCodigoProveedor() {
        return codigoProveedor;
    }

    public void setCodigoProveedor(int codigoProveedor) {
        this.codigoProveedor = codigoProveedor;
    }
    
    public String toString() {
        return getCodigoProducto() + " - " + getNombreProducto();
    }

}
