package org.julioherrera.system;

import java.io.InputStream;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.julioherrera.beans.Usuario;
import org.julioherrera.controllers.ClientesController;
import org.julioherrera.controllers.ComprasController;
import org.julioherrera.controllers.DetallesVentaController;
import org.julioherrera.controllers.EmailsClienteController;
import org.julioherrera.controllers.FacturasController;
import org.julioherrera.controllers.FormasPagoController;
import org.julioherrera.controllers.LoginController;
import org.julioherrera.controllers.MenuController;
import org.julioherrera.controllers.ProductosController;
import org.julioherrera.controllers.ProveedoresController;
import org.julioherrera.controllers.TelefonosClienteController;
import org.julioherrera.controllers.TelefonosProveedorController;
import org.julioherrera.controllers.TiposProductoController;
import org.julioherrera.controllers.UsuariosController;
import org.julioherrera.controllers.VentasController;

public class Main extends Application {
    
    private String VISTAS = "/org/julioherrera/views/";
    private Stage escenario;
    private Scene escena;
    private Usuario usuario;
    
    @Override
    public void start(Stage escenario) {
        this.escenario = escenario;
        escenario.setTitle("Amanecer");
        escenario.setResizable(false);
        login();
        escenario.getIcons().add(new Image("/org/julioherrera/resources/icon.jpg"));
        escenario.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    
    public void menu(Usuario usuario) {
        try{
            MenuController menuController = (MenuController) cambiarEscena("MenuView.fxml", 1000, 600);
            menuController.setEscenario(this);
            menuController.setUsuario(usuario);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void proveedores() {
        try{
            ProveedoresController proveedoresController = (ProveedoresController) cambiarEscena("ProveedoresView.fxml", 1000, 600);
            proveedoresController.setEscenario(this);
            proveedoresController.getEscenario().setUsuario(usuario);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void telefonosProveedor(int codigoProveedor) {
        try{
            TelefonosProveedorController telefonosProveedorController = (TelefonosProveedorController) cambiarEscena("TelefonosProveedorView.fxml", 1000, 600);
            telefonosProveedorController.setEscenario(this);
            telefonosProveedorController.setCodigoProveedor(codigoProveedor);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void tiposProducto() {
        try{
            TiposProductoController tiposProductoController = (TiposProductoController) cambiarEscena("TiposProductoView.fxml", 1000, 600);
            tiposProductoController.setEscenario(this);
            tiposProductoController.getEscenario().setUsuario(usuario);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void productos() {
        try{
            ProductosController productosController = (ProductosController) cambiarEscena("ProductosView.fxml", 1000, 600);
            productosController.setEscenario(this);
            productosController.getEscenario().setUsuario(usuario);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void compras() {
        try{
            ComprasController comprasController = (ComprasController) cambiarEscena("ComprasView.fxml", 1000, 600);
            comprasController.setEscenario(this);
            comprasController.getEscenario().setUsuario(usuario);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void formasPago() {
        try{
            FormasPagoController formasPagoController = (FormasPagoController) cambiarEscena("FormasPagoView.fxml", 1000, 600);
            formasPagoController.setEscenario(this);
            formasPagoController.getEscenario().setUsuario(usuario);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void clientes() {
        try{
            ClientesController clientesController = (ClientesController) cambiarEscena("ClientesView.fxml", 1000, 600);
            clientesController.setEscenario(this);
            clientesController.getEscenario().setUsuario(usuario);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void telefonosCliente(int codigoCliente) {
        try{
            TelefonosClienteController telefonosClienteController = (TelefonosClienteController) cambiarEscena("TelefonosClienteView.fxml", 1000, 600);
            telefonosClienteController.setEscenario(this);
            telefonosClienteController.setCodigoCliente(codigoCliente);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void emailsCliente(int codigoCliente) {
        try{
            EmailsClienteController emailsClienteController = (EmailsClienteController) cambiarEscena("EmailsClienteView.fxml", 1000, 600);
            emailsClienteController.setEscenario(this);
            emailsClienteController.setCodigoCliente(codigoCliente);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void usuarios() {
        try{
            UsuariosController usuariosController = (UsuariosController) cambiarEscena("UsuariosView.fxml", 1000, 600);
            usuariosController.setEscenario(this);
            usuariosController.getEscenario().setUsuario(usuario);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void ventas() {
        try{
            VentasController ventasController = (VentasController) cambiarEscena("VentasView.fxml", 1000, 600);
            ventasController.setEscenario(this);
            ventasController.getEscenario().setUsuario(usuario);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void detallesVenta(int codigoVenta, int codigoTipoProducto) {
        try{
            DetallesVentaController detallesVentaController = (DetallesVentaController) cambiarEscena("DetallesVentaView.fxml", 1000, 600);
            detallesVentaController.setEscenario(this);
            detallesVentaController.setCodigoVenta(codigoVenta);
            detallesVentaController.setCodigoTipoProducto(codigoTipoProducto);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void facturas() {
        try{
            FacturasController facturasController = (FacturasController) cambiarEscena("FacturasView.fxml", 1000, 600);
            facturasController.setEscenario(this);
            facturasController.getEscenario().setUsuario(usuario);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void login() {
        try{
            LoginController loginController = (LoginController) cambiarEscena("LoginView.fxml", 1000, 600);
            loginController.setEscenario(this);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public Initializable cambiarEscena (String fxml,int ancho,int alto) throws Exception{
        Initializable resultado = null;
        FXMLLoader cargadorFXML = new FXMLLoader();
        InputStream archivo = Main.class.getResourceAsStream(VISTAS + fxml);
        cargadorFXML.setBuilderFactory(new JavaFXBuilderFactory());
        cargadorFXML.setLocation(Main.class.getResource(VISTAS + fxml));
        escena = new Scene((AnchorPane) cargadorFXML.load(archivo),ancho,alto);
        escena.getStylesheets().add("/org/julioherrera/resources/StyleSheet.css");
        escenario.setScene(escena);
        escenario.sizeToScene();
        resultado = (Initializable)cargadorFXML.getController();
        return resultado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
}
