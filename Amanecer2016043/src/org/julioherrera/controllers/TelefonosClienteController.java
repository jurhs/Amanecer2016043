package org.julioherrera.controllers;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javax.swing.JOptionPane;
import org.julioherrera.beans.Cliente;
import org.julioherrera.beans.TelefonoCliente;
import org.julioherrera.db.Conexion;
import org.julioherrera.system.Main;

public class TelefonosClienteController implements Initializable {
    
    private Main escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones operacionActual = operaciones.NINGUNO;
    private ObservableList<TelefonoCliente> listaTelefonosCliente;
    private ObservableList<Cliente> listaClientes;
    private boolean camposCorrectos;
    private int codigoCliente;
    @FXML private TextField txtTelefono;
    @FXML private TextField txtDescripcion;
    @FXML private TableView tblTelefonosCliente;
    @FXML private TableColumn colCodigoTelefonoCliente;
    @FXML private TableColumn colTelefono;
    @FXML private TableColumn colDescripcion;
    @FXML private TableColumn colCodigoCliente;
    @FXML private Circle btnAgregar;
    @FXML private Circle btnActualizar;
    @FXML private Circle btnEliminar;
    @FXML private ImageView imgAgregar;
    @FXML private ImageView imgActualizar;
    @FXML private ImageView imgEliminar;
    @FXML private ImageView imgGuardarAgregar;
    @FXML private ImageView imgGuardarActualizar;
    @FXML private ImageView imgCancelar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        desactivarControles();
    }
    
    public void cargarDatos() {
        tblTelefonosCliente.setItems(getTelefonosCliente());
        colCodigoTelefonoCliente.setCellValueFactory(new PropertyValueFactory<TelefonoCliente, Integer>("codigoTelefonoCliente"));
        colTelefono.setCellValueFactory(new PropertyValueFactory<TelefonoCliente, String>("telefono"));
        colDescripcion.setCellValueFactory(new PropertyValueFactory<TelefonoCliente, String>("descripcion"));
        colCodigoCliente.setCellValueFactory(new PropertyValueFactory<TelefonoCliente, Integer>("codigoCliente"));
    }
    
    public ObservableList<TelefonoCliente> getTelefonosCliente() {
        ArrayList<TelefonoCliente> lista = new ArrayList<TelefonoCliente>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listTelefonosCliente(?)}");
            procedimiento.setInt(1, codigoCliente);
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new TelefonoCliente(resultado.getInt("codigoTelefonoCliente"), resultado.getString("telefono"), resultado.getString("descripcion"), resultado.getInt("codigoCliente")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaTelefonosCliente = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos() {
        if(tblTelefonosCliente.getSelectionModel().getSelectedIndex() > -1) {
            TelefonoCliente elemento = (TelefonoCliente)tblTelefonosCliente.getSelectionModel().getSelectedItem();
            txtTelefono.setText(elemento.getTelefono());
            txtDescripcion.setText(elemento.getDescripcion());
        }
    }
    
    public void btnAgregar() {
        switch(operacionActual) {
            case NINGUNO:
                activarControles();
                limpiarControles();
                imgAgregar.setVisible(false);
                imgEliminar.setVisible(false);
                btnActualizar.setVisible(false);
                operacionActual = operaciones.AGREGAR;
            break;
            case AGREGAR:
                agregar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgAgregar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnActualizar.setVisible(true);
                    operacionActual = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnActualizar() {
        switch(operacionActual) {
            case NINGUNO:
                if(tblTelefonosCliente.getSelectionModel().getSelectedItem() != null) {
                    activarControles();
                    imgActualizar.setVisible(false);
                    imgEliminar.setVisible(false);
                    btnAgregar.setVisible(false);
                    operacionActual = operaciones.ACTUALIZAR;
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Teléfono de Cliente");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgActualizar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnAgregar.setVisible(true);
                    operacionActual = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnEliminar() {
        switch(operacionActual) {
            case NINGUNO:
                if(tblTelefonosCliente.getSelectionModel().getSelectedItem() != null) {
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Teléfono de Cliente",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION) {
                        eliminar();
                        limpiarControles();
                        cargarDatos();
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Teléfono de Cliente");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar() {
        boolean entrar = true;
        int telefono = 0;
        try{
            telefono = Integer.parseInt((txtTelefono.getText()));
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "El campo 'Teléfono' debe de ser un número");
            entrar = false;
            camposCorrectos = false;
        }
        try{
            if(!(txtTelefono.getText().equals(""))&&!(txtDescripcion.getText().equals(""))&&(entrar)) {
                do{
                    PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_insertTelefonoCliente(?,?,?)}");
                    procedimiento.setString(1, (txtTelefono.getText())); //telefono
                    procedimiento.setString(2, (txtDescripcion.getText())); //descripcion
                    procedimiento.setInt(3, codigoCliente); //codigoCliente
                    if(telefono <= 0) {
                        JOptionPane.showMessageDialog(null, "El teléfono no puede ser 0 o negativo!");
                    }else {
                        procedimiento.execute();
                        camposCorrectos = true;
                        telefono = 0;
                    }
                }while(telefono > 0);
            }else {
                if(entrar) {
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    camposCorrectos = false;
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizar() {
        boolean entrar = true;
        int telefono = 0;
        try{
            telefono = Integer.parseInt((txtTelefono.getText()));
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "El campo 'Teléfono' debe de ser un número");
            entrar = false;
            camposCorrectos = false;
        }
        try{
            if(!(txtTelefono.getText().equals(""))&&!(txtDescripcion.getText().equals(""))&&(entrar)) {
                do{
                    PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_updateTelefonoCliente(?,?,?,?)}");
                    procedimiento.setString(1, (txtTelefono.getText())); //telefono
                    procedimiento.setString(2, (txtDescripcion.getText())); //descripcion
                    procedimiento.setInt(3, codigoCliente); //codigoCliente
                    procedimiento.setInt(4, (((TelefonoCliente)tblTelefonosCliente.getSelectionModel().getSelectedItem()).getCodigoTelefonoCliente())); //codigoTelefonoCliente
                    if(telefono <= 0) {
                        JOptionPane.showMessageDialog(null, "El teléfono no puede ser 0 o negativo!");
                    }else {
                        procedimiento.execute();
                        camposCorrectos = true;
                        telefono = 0;
                    }
                }while(telefono > 0);
            }else {
                if(entrar) {
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    camposCorrectos = false;
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar() {
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_deleteTelefonoCliente(?)}");
            procedimiento.setInt(1, (((TelefonoCliente)tblTelefonosCliente.getSelectionModel().getSelectedItem()).getCodigoTelefonoCliente())); //codigoTelefonoCliente
            procedimiento.execute();
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void desactivarControles() {
        txtTelefono.setDisable(true);
        txtDescripcion.setDisable(true);
        imgGuardarAgregar.setVisible(false);
        imgGuardarActualizar.setVisible(false);
        imgCancelar.setVisible(false);
    }
    
    public void activarControles() {
        txtTelefono.setDisable(false);
        txtDescripcion.setDisable(false);
        imgGuardarAgregar.setVisible(true);
        imgGuardarActualizar.setVisible(true);
        imgCancelar.setVisible(true);
    }
    
    public void limpiarControles() {
        txtTelefono.setText("");
        txtDescripcion.setText("");
    }
    
    public void cancelar() {
        tblTelefonosCliente.getSelectionModel().clearSelection();
        desactivarControles();
        limpiarControles();
        cargarDatos();
        imgAgregar.setVisible(true);
        imgActualizar.setVisible(true);
        imgEliminar.setVisible(true);
        btnAgregar.setVisible(true);
        btnActualizar.setVisible(true);
        operacionActual = operaciones.NINGUNO;
    }

    public void clientes() {
        this.escenario.clientes();
    }
    
    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
        cargarDatos();
    }

    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }
    
}
