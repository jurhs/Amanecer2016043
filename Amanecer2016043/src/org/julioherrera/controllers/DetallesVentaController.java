package org.julioherrera.controllers;

import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javax.swing.JOptionPane;
import org.julioherrera.beans.Cliente;
import org.julioherrera.beans.DetalleVenta;
import org.julioherrera.beans.Producto;
import org.julioherrera.beans.Venta;
import org.julioherrera.db.Conexion;
import org.julioherrera.system.Main;

public class DetallesVentaController implements Initializable {

    private Main escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones operacionActual = operaciones.NINGUNO;
    private ObservableList<DetalleVenta> listaDetallesVenta;
    private ObservableList<Venta> listaVentas;
    private ObservableList<Producto> listaProductos;
    private ObservableList<Cliente> listaClientes;
    private boolean camposCorrectos;
    private int codigoVenta;
    private int codigoTipoProducto;
    @FXML private TextField txtCantidad;
    @FXML private TextField txtPrecioVenta;
    @FXML private TextField txtProductoIva;
    @FXML private ComboBox cmbCodigoProducto;
    @FXML private TextField txtTotal;
    @FXML private TableView tblDetallesVenta;
    @FXML private TableColumn colCodigoDetalleVenta;
    @FXML private TableColumn colCantidad;
    @FXML private TableColumn colPrecioVenta;
    @FXML private TableColumn colProductoIva;
    @FXML private TableColumn colCodigoVenta;
    @FXML private TableColumn colCodigoProducto;
    @FXML private Circle btnAgregar;
    @FXML private Circle btnActualizar;
    @FXML private Circle btnEliminar;
    @FXML private ImageView imgAgregar;
    @FXML private ImageView imgActualizar;
    @FXML private ImageView imgEliminar;
    @FXML private ImageView imgGuardarAgregar;
    @FXML private ImageView imgGuardarActualizar;
    @FXML private ImageView imgCancelar;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        desactivarControles();
    }
    
    public void cargarDatos() {
        tblDetallesVenta.setItems(getDetallesVenta(codigoVenta));
        colCodigoDetalleVenta.setCellValueFactory(new PropertyValueFactory<DetalleVenta, Integer>("codigoDetalleVenta"));
        colCantidad.setCellValueFactory(new PropertyValueFactory<DetalleVenta, Integer>("cantidad"));
        colPrecioVenta.setCellValueFactory(new PropertyValueFactory<DetalleVenta, Double>("precioVenta"));
        colProductoIva.setCellValueFactory(new PropertyValueFactory<DetalleVenta, Double>("productoIva"));
        colCodigoVenta.setCellValueFactory(new PropertyValueFactory<DetalleVenta, Integer>("codigoVenta"));
        colCodigoProducto.setCellValueFactory(new PropertyValueFactory<DetalleVenta, Integer>("codigoProducto"));
    }
    
    public void cargarComboBox() {
        ArrayList<Producto> lista = new ArrayList<Producto>();
        getProductos();
        listaProductos.stream().filter((producto) -> (producto.getCodigoTipoProducto() == codigoTipoProducto)).forEachOrdered((producto) -> {
            lista.add(producto);
        });
        cmbCodigoProducto.setItems((FXCollections.observableArrayList(lista)));
        getVentas();
        getClientes();
    }
    
    public ObservableList<DetalleVenta> getDetallesVenta(int codigoVenta) {
        ArrayList<DetalleVenta> lista = new ArrayList<DetalleVenta>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listDetallesVenta(?)}");
            procedimiento.setInt(1, codigoVenta); //codigoVenta
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new DetalleVenta(resultado.getInt("codigoDetalleVenta"), resultado.getInt("cantidad"), resultado.getDouble("precioVenta"), resultado.getDouble("productoIva"), resultado.getInt("codigoVenta"), resultado.getInt("codigoProducto")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaDetallesVenta = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Producto> getProductos() {
        ArrayList<Producto> lista = new ArrayList<Producto>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listProductos}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Producto(resultado.getInt("codigoProducto"), resultado.getString("nombreProducto"), resultado.getInt("existencia"), resultado.getDouble("precioCosto"), resultado.getDouble("precioVenta"), resultado.getDouble("productoIva"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion"), resultado.getInt("codigoTipoProducto"), resultado.getInt("codigoProveedor")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaProductos = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Venta> getVentas() {
        ArrayList<Venta> lista = new ArrayList<Venta>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listVentas}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Venta(resultado.getInt("codigoVenta"), resultado.getDate("fechaVenta"), resultado.getInt("iva"), resultado.getString("observacion"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion"), resultado.getDouble("total"), resultado.getInt("numeroFactura"), resultado.getInt("codigoCliente"), resultado.getInt("codigoFormaPago"), resultado.getInt("codigoTipoProducto"), resultado.getInt("codigoUsuario")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaVentas = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Cliente> getClientes() {
        ArrayList<Cliente> lista = new ArrayList<Cliente>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listClientes}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Cliente(resultado.getInt("codigoCliente"), resultado.getString("nombresCliente"), resultado.getString("apellidosCliente"), resultado.getString("direccionCliente"), resultado.getString("nitCliente"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaClientes = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos() {
        if(tblDetallesVenta.getSelectionModel().getSelectedIndex() > -1) {
            DetalleVenta elemento = (DetalleVenta)tblDetallesVenta.getSelectionModel().getSelectedItem();
            txtCantidad.setText(String.valueOf(elemento.getCantidad()));
            txtPrecioVenta.setText(String.valueOf(elemento.getPrecioVenta()));
            txtProductoIva.setText(String.valueOf(elemento.getProductoIva()));
            listaProductos.stream().filter((producto) -> (producto.getCodigoProducto() == elemento.getCodigoProducto())).forEachOrdered((producto) -> {
                cmbCodigoProducto.setValue(producto);
            });
            txtTotal.setText(String.valueOf(Double.parseDouble(txtPrecioVenta.getText()) * Integer.parseInt(txtCantidad.getText())));
        }
    }
    
    public void setTextFields() {
        try{
            if((cmbCodigoProducto.getSelectionModel().getSelectedItem())!= null){
                Double precio = null;
                Double iva = null;
                Double total = null;
                int cantidad;
                cantidad = Integer.parseInt(txtCantidad.getText());
                precio = (((Producto)cmbCodigoProducto.getSelectionModel().getSelectedItem()).getPrecioVenta());
                iva = (((Producto)cmbCodigoProducto.getSelectionModel().getSelectedItem()).getPrecioCosto()) * 0.12;
                total = (precio * cantidad);
                txtPrecioVenta.setText(String.valueOf(precio));
                txtProductoIva.setText(String.valueOf(iva));
            }
        }catch(Exception e){
            txtCantidad.setText("0");
        }
    }
    
    public void setTotal() {
        if(!(txtCantidad.getText().equals(""))&&!(txtPrecioVenta.getText().equals(""))) {
            int cantidad;
            double precioVenta;
            try{
                cantidad = Integer.parseInt(txtCantidad.getText());
            }catch(Exception e) {
                cantidad = 0;
            }
            precioVenta = Double.parseDouble(txtPrecioVenta.getText());
            txtTotal.setText(String.valueOf(cantidad * precioVenta));
        }
    }
    
    public Venta getVenta(int codigoVenta) {
        Venta elemento = new Venta();
        for(Venta venta: listaVentas) {
            if(venta.getCodigoVenta() == codigoVenta) {
                elemento = venta;
            }
        }
        return elemento;
    }
    
    public Cliente getCliente(int codigoCliente) {
        Cliente elemento = new Cliente();
        for(Cliente cliente: listaClientes) {
            if(cliente.getCodigoCliente() == codigoCliente) {
                elemento = cliente;
            }
        }
        return elemento;
    }
    
    public void btnAgregar() {
        switch(operacionActual) {
            case NINGUNO:
                activarControles();
                limpiarControles();
                imgAgregar.setVisible(false);
                imgEliminar.setVisible(false);
                btnActualizar.setVisible(false);
                operacionActual = operaciones.AGREGAR;
            break;
            case AGREGAR:
                agregar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgAgregar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnActualizar.setVisible(true);
                    operacionActual = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnActualizar() {
        switch(operacionActual) {
            case NINGUNO:
                if(tblDetallesVenta.getSelectionModel().getSelectedItem() != null) {
                    String verificacion = JOptionPane.showInputDialog(null, "Para Actualizar el registro ingrese contraseña de administrador","Actualizar Detalle Venta",JOptionPane.INFORMATION_MESSAGE);
                    if(verificacion.equals("admin")) {
                        activarControles();
                        imgActualizar.setVisible(false);
                        imgEliminar.setVisible(false);
                        btnAgregar.setVisible(false);
                        operacionActual = operaciones.ACTUALIZAR;
                    }else {
                        JOptionPane.showMessageDialog(null, "Verifiación de Administrador no aprobada!");
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Detalle de Venta");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgActualizar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnAgregar.setVisible(true);
                    operacionActual = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnEliminar() {
        switch(operacionActual) {
            case NINGUNO:
                if(tblDetallesVenta.getSelectionModel().getSelectedItem() != null) {
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Detalle Venta",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION) {
                        String verificacion = JOptionPane.showInputDialog(null, "Para Actualizar el registro ingrese contraseña de administrador","Actualizar Detalle Venta",JOptionPane.INFORMATION_MESSAGE);
                        if(verificacion.equals("admin")) {
                            eliminar();
                            limpiarControles();
                            cargarDatos();
                        }else {
                            JOptionPane.showMessageDialog(null, "Verifiación de Administrador no aprobada!");
                        }
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Detalle de Venta");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar() {
        boolean entrar = true;
        int cantidad = 0;
        try{
            cantidad = Integer.parseInt((txtCantidad.getText()));
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "El campo 'Cantidad' debe de ser un número");
            entrar = false;
            camposCorrectos = false;
        }
        try{
            if(!(txtCantidad.getText().equals(""))&&!(txtPrecioVenta.getText().equals(""))&&!(txtProductoIva.getText().equals(""))&&!(cmbCodigoProducto.getValue().equals(""))&&(entrar)) {
                do {
                    PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_insertDetalleVenta(?,?,?,?,?,?,?,?,?,?)}");
                    procedimiento.setInt(1, (Integer.parseInt(txtCantidad.getText()))); //cantidad
                    procedimiento.setDouble(2, (Double.parseDouble(txtPrecioVenta.getText()))); //precioVenta
                    procedimiento.setDouble(3, (Double.parseDouble(txtProductoIva.getText()))); //productoIva
                    procedimiento.setInt(4, (codigoVenta)); //codigoVenta
                    procedimiento.setInt(5, (((Producto)cmbCodigoProducto.getSelectionModel().getSelectedItem()).getCodigoProducto())); //codigoProducto
                    procedimiento.setInt(6, (getVenta(codigoVenta).getNumeroFactura())); //numeroFactura
                    procedimiento.setDate(7, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(getVenta(codigoVenta).getFechaVenta())))); //fecha
                    procedimiento.setString(8, (getCliente(getVenta(codigoVenta).getCodigoCliente()).getNitCliente())); //nitCliente
                    procedimiento.setDouble(9, (Double.parseDouble(txtTotal.getText()))); //total
                    procedimiento.setInt(10, (getVenta(codigoVenta).getCodigoCliente())); //codigoCliente
                    if(cantidad <= 0) {
                        JOptionPane.showMessageDialog(null, "La cantidad no puede ser 0 o negativa!");
                    }
                    if(cantidad > 0) {
                        Producto producto = (Producto)cmbCodigoProducto.getSelectionModel().getSelectedItem();
                        if((cantidad) > (producto.getExistencia())) {
                            JOptionPane.showMessageDialog(null, "La cantidad es mayor a la existencia del producto!");
                            cantidad = 0;
                        }else {
                            procedimiento.execute();
                            camposCorrectos = true;
                            cantidad = 0;
                        }
                    }
                }while(cantidad > 0);
            }else {
                if(entrar) {
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    camposCorrectos = false;
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizar() {
        boolean entrar = true;
        int cantidad = 0;
        try{
            cantidad = Integer.parseInt((txtCantidad.getText()));
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "El campo 'Cantidad' debe de ser un número");
            entrar = false;
            camposCorrectos = false;
        }
        try{
            if(!(txtCantidad.getText().equals(""))&&!(txtPrecioVenta.getText().equals(""))&&!(txtProductoIva.getText().equals(""))&&!(cmbCodigoProducto.getValue().equals(""))&&(entrar)) {
                do{
                    PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_updateDetalleVenta(?,?,?,?,?,?,?,?,?,?,?,?)}");
                    procedimiento.setInt(1, (Integer.parseInt(txtCantidad.getText()))); //cantidad
                    procedimiento.setDouble(2, (Double.parseDouble(txtPrecioVenta.getText()))); //precioVenta
                    procedimiento.setDouble(3, (Double.parseDouble(txtProductoIva.getText()))); //productoIva
                    procedimiento.setInt(4, (codigoVenta)); //codigoVenta
                    procedimiento.setInt(5, (((Producto)cmbCodigoProducto.getSelectionModel().getSelectedItem()).getCodigoProducto())); //codigoProducto
                    procedimiento.setInt(6, (((DetalleVenta)tblDetallesVenta.getSelectionModel().getSelectedItem()).getCodigoDetalleVenta())); //codigoDetalleVenta
                    procedimiento.setInt(7, (getVenta(codigoVenta).getNumeroFactura())); //numeroFactura
                    procedimiento.setDate(8, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(getVenta(codigoVenta).getFechaVenta())))); //fecha
                    procedimiento.setString(9, (getCliente(getVenta(codigoVenta).getCodigoCliente()).getNitCliente())); //nitCliente
                    procedimiento.setDouble(10, (Double.parseDouble(txtTotal.getText()))); //total
                    procedimiento.setInt(11, (getVenta(codigoVenta).getCodigoCliente())); //codigoCliente
                    try{
                        int codigoFactura = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese un correcto CÓDIGO DE FACTURA","Actualizar Detalle de Venta",JOptionPane.INFORMATION_MESSAGE));
                        procedimiento.setInt(12, (codigoFactura)); //codigoFactura
                        if(cantidad <= 0) {
                            JOptionPane.showMessageDialog(null, "La cantidad no puede ser 0 o negativa!");
                        }
                        if(cantidad > 0) {
                            Producto producto = (Producto)cmbCodigoProducto.getSelectionModel().getSelectedItem();
                            if((cantidad) > (producto.getExistencia())) {
                                JOptionPane.showMessageDialog(null, "La cantidad es mayor a la existencia del producto!");
                                cantidad = 0;
                            }else {
                                procedimiento.execute();
                                camposCorrectos = true;
                                cantidad = 0;
                            }
                        }
                    }catch(SQLException e) {
                        JOptionPane.showMessageDialog(null, "el Código de Factura ingresado no es válido");
                        cantidad = 0;
                    }catch(Exception e) {
                        JOptionPane.showMessageDialog(null, "el Código de Factura ingresado no es válido");
                        cantidad = 0;
                    }
                }while(cantidad > 0);
            }else {
                if(entrar) {
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    camposCorrectos = false;
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar() {
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_deleteDetalleVenta(?,?)}");
            procedimiento.setInt(1, (((DetalleVenta)tblDetallesVenta.getSelectionModel().getSelectedItem()).getCodigoDetalleVenta())); //codigoDetalleVenta
            try{
                int codigoFactura = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese un correcto CÓDIGO DE FACTURA","Eliminar Detalle de Venta",JOptionPane.INFORMATION_MESSAGE));
                procedimiento.setInt(12, (codigoFactura)); //codigoFactura
                procedimiento.execute();
            }catch(SQLException e) {
                JOptionPane.showMessageDialog(null, "el Código de Factura ingresado no es válido");
            }catch(Exception e) {
                JOptionPane.showMessageDialog(null, "el Código de Factura ingresado no es válido");
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void desactivarControles() {
        txtCantidad.setDisable(true);
        txtPrecioVenta.setDisable(true);
        txtProductoIva.setDisable(true);
        cmbCodigoProducto.setDisable(true);
        txtTotal.setDisable(true);
        imgGuardarAgregar.setVisible(false);
        imgGuardarActualizar.setVisible(false);
        imgCancelar.setVisible(false);
    }
    
    public void activarControles() {
        txtCantidad.setDisable(false);
        cmbCodigoProducto.setDisable(false);
        txtTotal.setDisable(false);
        txtTotal.setEditable(false);
        imgGuardarAgregar.setVisible(true);
        imgGuardarActualizar.setVisible(true);
        imgCancelar.setVisible(true);
    }
    
    public void limpiarControles() {
        txtCantidad.setText("");
        txtPrecioVenta.setText("");
        txtProductoIva.setText("");
        cmbCodigoProducto.setValue("");
        txtTotal.setText("");
    }
    
    public void cancelar() {
        tblDetallesVenta.getSelectionModel().clearSelection();
        desactivarControles();
        limpiarControles();
        cargarDatos();
        imgAgregar.setVisible(true);
        imgActualizar.setVisible(true);
        imgEliminar.setVisible(true);
        btnAgregar.setVisible(true);
        btnActualizar.setVisible(true);
        operacionActual = operaciones.NINGUNO;
    }
    
    public void ventas() {
        this.escenario.ventas();
    }

    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
        cargarDatos();
    }

    public int getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    public void setCodigoTipoProducto(int codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
        cargarComboBox();
    }
    
}
