package org.julioherrera.controllers;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javax.swing.JOptionPane;
import org.julioherrera.beans.Cliente;
import org.julioherrera.beans.EmailCliente;
import org.julioherrera.db.Conexion;
import org.julioherrera.system.Main;

public class EmailsClienteController implements Initializable {
    
    private Main escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones operacionActual = operaciones.NINGUNO;
    private ObservableList<EmailCliente> listaEmailsCliente;
    private ObservableList<Cliente> listaClientes;
    private boolean camposCorrectos;
    private int codigoCliente;
    @FXML private TextField txtEmail;
    @FXML private TextField txtDescripcion;
    @FXML private TableView tblEmailsCliente;
    @FXML private TableColumn colCodigoEmailCliente;
    @FXML private TableColumn colEmail;
    @FXML private TableColumn colDescripcion;
    @FXML private TableColumn colCodigoCliente;
    @FXML private Circle btnAgregar;
    @FXML private Circle btnActualizar;
    @FXML private Circle btnEliminar;
    @FXML private ImageView imgAgregar;
    @FXML private ImageView imgActualizar;
    @FXML private ImageView imgEliminar;
    @FXML private ImageView imgGuardarAgregar;
    @FXML private ImageView imgGuardarActualizar;
    @FXML private ImageView imgCancelar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        desactivarControles();
    }
    
    public void cargarDatos() {
        tblEmailsCliente.setItems(getEmailsCliente());
        colCodigoEmailCliente.setCellValueFactory(new PropertyValueFactory<EmailCliente, Integer>("codigoEmailCliente"));
        colEmail.setCellValueFactory(new PropertyValueFactory<EmailCliente, String>("email"));
        colDescripcion.setCellValueFactory(new PropertyValueFactory<EmailCliente, String>("descripcion"));
        colCodigoCliente.setCellValueFactory(new PropertyValueFactory<EmailCliente, Integer>("codigoCliente"));
    }
    
    public ObservableList<EmailCliente> getEmailsCliente() {
        ArrayList<EmailCliente> lista = new ArrayList<EmailCliente>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listEmailsCliente(?)}");
            procedimiento.setInt(1, codigoCliente);
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new EmailCliente(resultado.getInt("codigoEmailCliente"), resultado.getString("email"), resultado.getString("descripcion"), resultado.getInt("codigoCliente")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaEmailsCliente = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos() {
        if(tblEmailsCliente.getSelectionModel().getSelectedIndex() > -1) {
            EmailCliente elemento = (EmailCliente)tblEmailsCliente.getSelectionModel().getSelectedItem();
            txtEmail.setText(elemento.getEmail());
            txtDescripcion.setText(elemento.getDescripcion());
        }
    }
    
    public void btnAgregar() {
        switch(operacionActual) {
            case NINGUNO:
                activarControles();
                limpiarControles();
                imgAgregar.setVisible(false);
                imgEliminar.setVisible(false);
                btnActualizar.setVisible(false);
                operacionActual = operaciones.AGREGAR;
            break;
            case AGREGAR:
                agregar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgAgregar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnActualizar.setVisible(true);
                    operacionActual = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnActualizar() {
        switch(operacionActual) {
            case NINGUNO:
                if(tblEmailsCliente.getSelectionModel().getSelectedItem() != null) {
                    activarControles();
                    imgActualizar.setVisible(false);
                    imgEliminar.setVisible(false);
                    btnAgregar.setVisible(false);
                    operacionActual = operaciones.ACTUALIZAR;
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Email de Cliente");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgActualizar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnAgregar.setVisible(true);
                    operacionActual = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnEliminar() {
        switch(operacionActual) {
            case NINGUNO:
                if(tblEmailsCliente.getSelectionModel().getSelectedItem() != null) {
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Email de Cliente",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION) {
                        eliminar();
                        limpiarControles();
                        cargarDatos();
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Email de Cliente");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar() {
        try{
            if(!(txtEmail.getText().equals(""))&&!(txtDescripcion.getText().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_insertEmailCliente(?,?,?)}");
                procedimiento.setString(1, (txtEmail.getText())); //email
                procedimiento.setString(2, (txtDescripcion.getText())); //descripcion
                procedimiento.setInt(3, codigoCliente); //codigoCliente
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizar() {
        try{
            if(!(txtEmail.getText().equals(""))&&!(txtDescripcion.getText().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_updateEmailCliente(?,?,?,?)}");
                procedimiento.setString(1, (txtEmail.getText())); //email
                procedimiento.setString(2, (txtDescripcion.getText())); //descripcion
                procedimiento.setInt(3, codigoCliente); //codigoCliente
                procedimiento.setInt(4, (((EmailCliente)tblEmailsCliente.getSelectionModel().getSelectedItem()).getCodigoEmailCliente())); //codigoEmailCliente
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar() {
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_deleteEmailCliente(?)}");
            procedimiento.setInt(1, (((EmailCliente)tblEmailsCliente.getSelectionModel().getSelectedItem()).getCodigoEmailCliente())); //codigoEmailCliente
            procedimiento.execute();
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void desactivarControles() {
        txtEmail.setDisable(true);
        txtDescripcion.setDisable(true);
        imgGuardarAgregar.setVisible(false);
        imgGuardarActualizar.setVisible(false);
        imgCancelar.setVisible(false);
    }
    
    public void activarControles() {
        txtEmail.setDisable(false);
        txtDescripcion.setDisable(false);
        imgGuardarAgregar.setVisible(true);
        imgGuardarActualizar.setVisible(true);
        imgCancelar.setVisible(true);
    }
    
    public void limpiarControles() {
        txtEmail.setText("");
        txtDescripcion.setText("");
    }
    
    public void cancelar() {
        tblEmailsCliente.getSelectionModel().clearSelection();
        desactivarControles();
        limpiarControles();
        cargarDatos();
        imgAgregar.setVisible(true);
        imgActualizar.setVisible(true);
        imgEliminar.setVisible(true);
        btnAgregar.setVisible(true);
        btnActualizar.setVisible(true);
        operacionActual = operaciones.NINGUNO;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
        cargarDatos();
    }
    
    public void clientes() {
        this.escenario.clientes();
    }

    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }
    
}
