package org.julioherrera.controllers;

import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.StringConverter;
import javax.swing.JOptionPane;
import org.julioherrera.beans.Cliente;
import org.julioherrera.beans.Factura;
import org.julioherrera.beans.Producto;
import org.julioherrera.db.Conexion;
import org.julioherrera.reports.GenerarReporte;
import org.julioherrera.system.Main;

public class FacturasController implements Initializable {
    
    private Main escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones operacionActual = operaciones.NINGUNO;
    private ObservableList<Factura> listaFacturas;
    private ObservableList<Producto> listaProductos;
    private ObservableList<Cliente> listaClientes;
    @FXML private TableView tblFacturas;
    @FXML private TableColumn colCodigoFactura;
    @FXML private TableColumn colNumeroFactura;
    @FXML private TableColumn colFecha;
    @FXML private TableColumn colNitCliente;
    @FXML private TableColumn colCantidad;
    @FXML private TableColumn colPrecioVenta;
    @FXML private TableColumn colTotal;
    @FXML private TableColumn colCodigoProducto;
    @FXML private TableColumn colCodigoCliente;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
    }
    
    public void cargarDatos() {
        tblFacturas.setItems(getFacturas());
        colCodigoFactura.setCellValueFactory(new PropertyValueFactory<Factura, Integer>("codigoFactura"));
        colNumeroFactura.setCellValueFactory(new PropertyValueFactory<Factura, String>("numeroFactura"));
        colFecha.setCellValueFactory(new PropertyValueFactory<Factura, Date>("fecha"));
        colNitCliente.setCellValueFactory(new PropertyValueFactory<Factura, String>("nitCliente"));
        colCantidad.setCellValueFactory(new PropertyValueFactory<Factura, Integer>("cantidad"));
        colPrecioVenta.setCellValueFactory(new PropertyValueFactory<Factura, Double>("precioVenta"));
        colTotal.setCellValueFactory(new PropertyValueFactory<Factura, Double>("total"));
        colCodigoProducto.setCellValueFactory(new PropertyValueFactory<Factura, Integer>("codigoProducto"));
        colCodigoCliente.setCellValueFactory(new PropertyValueFactory<Factura, Integer>("codigoCliente"));
    }
    
    public ObservableList<Factura> getFacturas() {
        ArrayList<Factura> lista = new ArrayList<Factura>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listFacturas}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Factura(resultado.getInt("codigoFactura"), resultado.getString("numeroFactura"), resultado.getDate("fecha"), resultado.getString("nitCliente"), resultado.getInt("cantidad"), resultado.getDouble("precioVenta"), resultado.getDouble("total"), resultado.getInt("codigoProducto"), resultado.getInt("codigoCliente")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaFacturas = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Producto> getProductos() {
        ArrayList<Producto> lista = new ArrayList<Producto>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listProductos}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Producto(resultado.getInt("codigoProducto"), resultado.getString("nombreProducto"), resultado.getInt("existencia"), resultado.getDouble("precioCosto"), resultado.getDouble("precioVenta"), resultado.getDouble("productoIva"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion"), resultado.getInt("codigoTipoProducto"), resultado.getInt("codigoProveedor")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaProductos = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Cliente> getClientes() {
        ArrayList<Cliente> lista = new ArrayList<Cliente>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listClientes}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Cliente(resultado.getInt("codigoCliente"), resultado.getString("nombresCliente"), resultado.getString("apellidosCliente"), resultado.getString("direccionCliente"), resultado.getString("nitCliente"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaClientes = FXCollections.observableArrayList(lista);
    }

    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }
    
    public void menu() {
        this.escenario.menu(this.escenario.getUsuario());
    }
    
    public void proveedores() {
        this.escenario.proveedores();
    }
    
    public void tiposProducto() {
        this.escenario.tiposProducto();
    }
    
    public void productos() {
        this.escenario.productos();
    }
    
    public void compras() {
        this.escenario.compras();
    }
    
    public void formasPago() {
        this.escenario.formasPago();
    }
    
    public void clientes() {
        this.escenario.clientes();
    }
    
    public void usuarios() {
        this.escenario.usuarios();
    }
    
    public void ventas() {
        this.escenario.ventas();
    }
    
    public void facturas() {
        this.escenario.facturas();
    }

}
