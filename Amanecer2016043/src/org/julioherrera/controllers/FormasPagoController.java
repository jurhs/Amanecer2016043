package org.julioherrera.controllers;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javax.swing.JOptionPane;
import org.julioherrera.beans.FormaPago;
import org.julioherrera.db.Conexion;
import org.julioherrera.system.Main;

public class FormasPagoController implements Initializable {
    
    private Main escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<FormaPago> listaFormasPago;
    private boolean camposCorrectos;
    @FXML private TextField txtDescripcion;
    @FXML private TableView tblFormasPago;
    @FXML private TableColumn colCodigoFormaPago;
    @FXML private TableColumn colDescripcion;
    @FXML private Circle btnAgregar;
    @FXML private Circle btnActualizar;
    @FXML private Circle btnEliminar;
    @FXML private ImageView imgAgregar;
    @FXML private ImageView imgActualizar;
    @FXML private ImageView imgEliminar;
    @FXML private ImageView imgGuardarAgregar;
    @FXML private ImageView imgGuardarActualizar;
    @FXML private ImageView imgCancelar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
        desactivarControles();
    }
    
    public void cargarDatos() {
        tblFormasPago.setItems(getFormasPago());
        colCodigoFormaPago.setCellValueFactory(new PropertyValueFactory<FormaPago, Integer>("codigoFormaPago"));
        colDescripcion.setCellValueFactory(new PropertyValueFactory<FormaPago, String>("descripcion"));
    }
    
    public ObservableList<FormaPago> getFormasPago() {
        ArrayList<FormaPago> lista = new ArrayList<FormaPago>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listFormasPago}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new FormaPago(resultado.getInt("codigoFormaPago"), resultado.getString("descripcion")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaFormasPago = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos() {
        if(tblFormasPago.getSelectionModel().getSelectedIndex() > -1) {
            txtDescripcion.setText(((FormaPago)tblFormasPago.getSelectionModel().getSelectedItem()).getDescripcion());
        }
    }
    
    public void btnAgregar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                activarControles();
                limpiarControles();
                imgAgregar.setVisible(false);
                imgEliminar.setVisible(false);
                btnActualizar.setVisible(false);
                tipoOperaciones = operaciones.AGREGAR;
            break;
            case AGREGAR:
                agregar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgAgregar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnActualizar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnActualizar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblFormasPago.getSelectionModel().getSelectedItem() != null) {
                    activarControles();
                    imgActualizar.setVisible(false);
                    imgEliminar.setVisible(false);
                    btnAgregar.setVisible(false);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar una Forma de Pago");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgActualizar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnAgregar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnEliminar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblFormasPago.getSelectionModel().getSelectedItem() != null) {
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?", "Eliminar Forma de Pago", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION) {
                        eliminar();
                        limpiarControles();
                        cargarDatos();
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar una Forma de Pago");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar() {
        try{
            if(!(txtDescripcion.getText().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_insertFormaPago(?)}");
                procedimiento.setString(1, txtDescripcion.getText()); //descripcion
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizar() {
        try{
            if(!(txtDescripcion.getText().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_updateFormaPago(?,?)}");
                procedimiento.setString(1, txtDescripcion.getText()); //descripcion
                procedimiento.setInt(2, (((FormaPago)tblFormasPago.getSelectionModel().getSelectedItem()).getCodigoFormaPago())); //codigoFormaPago
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar() {
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_deleteFormaPago(?)}");
            procedimiento.setInt(1, (((FormaPago)tblFormasPago.getSelectionModel().getSelectedItem()).getCodigoFormaPago()));
            procedimiento.execute();
        }catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Un registro depende de esta Forma de Pago");
        }
    }
    
    public void desactivarControles() {
        txtDescripcion.setDisable(true);
        imgGuardarAgregar.setVisible(false);
        imgGuardarActualizar.setVisible(false);
        imgCancelar.setVisible(false);
    }
    
    public void activarControles() {
        txtDescripcion.setDisable(false);
        imgGuardarAgregar.setVisible(true);
        imgGuardarActualizar.setVisible(true);
        imgCancelar.setVisible(true);
    }
    
    public void limpiarControles() {
        txtDescripcion.setText("");
    }
    
    public void cancelar() {
        tblFormasPago.getSelectionModel().clearSelection();
        desactivarControles();
        limpiarControles();
        cargarDatos();
        imgAgregar.setVisible(true);
        imgActualizar.setVisible(true);
        imgEliminar.setVisible(true);
        btnAgregar.setVisible(true);
        btnActualizar.setVisible(true);
        tipoOperaciones = operaciones.NINGUNO;
    }

    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }
    
    public void menu() {
        this.escenario.menu(this.escenario.getUsuario());
    }
    
    public void proveedores() {
        this.escenario.proveedores();
    }
    
    public void tiposProducto() {
        this.escenario.tiposProducto();
    }
    
    public void productos() {
        this.escenario.productos();
    }
    
    public void compras() {
        this.escenario.compras();
    }
    
    public void formasPago() {
        this.escenario.formasPago();
    }
    
    public void clientes() {
        this.escenario.clientes();
    }
    
    public void usuarios() {
        this.escenario.usuarios();
    }
    
    public void ventas() {
        this.escenario.ventas();
    }
    
    public void facturas() {
        this.escenario.facturas();
    }
    
}
