package org.julioherrera.controllers;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javax.swing.JOptionPane;
import org.julioherrera.beans.Usuario;
import org.julioherrera.db.Conexion;
import org.julioherrera.system.Main;

public class UsuariosController implements Initializable {

    private Main escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<Usuario> listaUsuarios;
    private boolean camposCorrectos;
    @FXML private TextField txtNombresUsuario;
    @FXML private TextField txtApellidosUsuario;
    @FXML private TextField txtUsuario;
    @FXML private TextField txtContrasena;
    @FXML private TableView tblUsuarios;
    @FXML private TableColumn colCodigoUsuario;
    @FXML private TableColumn colNombresUsuario;
    @FXML private TableColumn colApellidosUsuario;
    @FXML private TableColumn colUsuario;
    @FXML private Circle btnAgregar;
    @FXML private Circle btnActualizar;
    @FXML private Circle btnEliminar;
    @FXML private ImageView imgAgregar;
    @FXML private ImageView imgActualizar;
    @FXML private ImageView imgEliminar;
    @FXML private ImageView imgGuardarAgregar;
    @FXML private ImageView imgGuardarActualizar;
    @FXML private ImageView imgCancelar;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
        desactivarControles();
    }
    
    public void cargarDatos() {
        tblUsuarios.setItems(getUsuarios());
        colCodigoUsuario.setCellValueFactory(new PropertyValueFactory<Usuario, Integer>("codigoUsuario"));
        colNombresUsuario.setCellValueFactory(new PropertyValueFactory<Usuario, String>("nombresUsuario"));
        colApellidosUsuario.setCellValueFactory(new PropertyValueFactory<Usuario, String>("apellidosUsuario"));
        colUsuario.setCellValueFactory(new PropertyValueFactory<Usuario, String>("usuario"));
    }
    
    public ObservableList<Usuario> getUsuarios() {
        ArrayList<Usuario> lista = new ArrayList<Usuario>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listUsuarios}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Usuario(resultado.getInt("codigoUsuario"), resultado.getString("nombresUsuario"), resultado.getString("apellidosUsuario"), resultado.getString("usuario"), resultado.getString("contrasena")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaUsuarios = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos() {
        if(tblUsuarios.getSelectionModel().getSelectedIndex() > -1) {
            Usuario elemento = (Usuario)tblUsuarios.getSelectionModel().getSelectedItem();
            txtNombresUsuario.setText(elemento.getNombresUsuario());
            txtApellidosUsuario.setText(elemento.getApellidosUsuario());
            txtUsuario.setText(elemento.getUsuario());
        }
    }
    
    public void btnAgregar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                activarControles();
                limpiarControles();
                imgAgregar.setVisible(false);
                imgEliminar.setVisible(false);
                btnActualizar.setVisible(false);
                tipoOperaciones = operaciones.AGREGAR;
            break;
            case AGREGAR:
                agregar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgAgregar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnActualizar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnActualizar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblUsuarios.getSelectionModel().getSelectedItem() != null) {
                    activarControles();
                    imgActualizar.setVisible(false);
                    imgEliminar.setVisible(false);
                    btnAgregar.setVisible(false);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Usuario");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgActualizar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnAgregar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnEliminar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblUsuarios.getSelectionModel().getSelectedItem() != null) {
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?", "Eliminar Usuario", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION) {
                        eliminar();
                        limpiarControles();
                        cargarDatos();
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Usuario");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar() {
        try{
            if(!(txtNombresUsuario.getText().equals(""))&&!(txtApellidosUsuario.getText().equals(""))&&!(txtUsuario.getText().equals(""))&&!(txtContrasena.getText().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_insertUsuario(?,?,?,?)}");
                procedimiento.setString(1, (txtNombresUsuario.getText())); //nombresUsuario
                procedimiento.setString(2, (txtApellidosUsuario.getText()));//apellidosUsuario
                procedimiento.setString(3, (txtUsuario.getText())); //usuario
                procedimiento.setString(4, (txtContrasena.getText())); //contraseña
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizar() {
        try {
            if(!(txtNombresUsuario.getText().equals(""))&&!(txtApellidosUsuario.getText().equals(""))&&!(txtUsuario.getText().equals(""))&&!(txtContrasena.getText().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_updateUsuario(?,?,?,?,?)}");
                procedimiento.setString(1, (txtNombresUsuario.getText())); //nombresUsuario
                procedimiento.setString(2, (txtApellidosUsuario.getText())); //apellidosUsuario
                procedimiento.setString(3, (txtUsuario.getText())); //usario
                procedimiento.setString(4, (txtContrasena.getText())); //contraseña
                procedimiento.setInt(5, (((Usuario)tblUsuarios.getSelectionModel().getSelectedItem()).getCodigoUsuario())); //codigoUsuario
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar() {
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_deleteUsuario(?)}");
            procedimiento.setInt(1, (((Usuario)tblUsuarios.getSelectionModel().getSelectedItem()).getCodigoUsuario())); //codigoUsuario
            procedimiento.execute();
        }catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Un registro depende de este Usuario");
        }
    }
    
    public void desactivarControles() {
        txtNombresUsuario.setDisable(true);
        txtApellidosUsuario.setDisable(true);
        txtUsuario.setDisable(true);
        txtContrasena.setDisable(true);
        imgGuardarAgregar.setVisible(false);
        imgGuardarActualizar.setVisible(false);
        imgCancelar.setVisible(false);
    }
    
    public void activarControles() {
        txtNombresUsuario.setDisable(false);
        txtApellidosUsuario.setDisable(false);
        txtUsuario.setDisable(false);
        txtContrasena.setDisable(false);
        imgGuardarAgregar.setVisible(true);
        imgGuardarActualizar.setVisible(true);
        imgCancelar.setVisible(true);
    }
    
    public void limpiarControles() {
        txtNombresUsuario.setText("");
        txtApellidosUsuario.setText("");
        txtUsuario.setText("");
        txtContrasena.setText("");
    }
    
    public void cancelar() {
        tblUsuarios.getSelectionModel().clearSelection();
        desactivarControles();
        limpiarControles();
        cargarDatos();
        imgAgregar.setVisible(true);
        imgActualizar.setVisible(true);
        imgEliminar.setVisible(true);
        btnAgregar.setVisible(true);
        btnActualizar.setVisible(true);
        tipoOperaciones = operaciones.NINGUNO;
    }

    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }
    
    public void menu() {
        this.escenario.menu(this.escenario.getUsuario());
    }
    
    public void proveedores() {
        this.escenario.proveedores();
    }
    
    public void tiposProducto() {
        this.escenario.tiposProducto();
    }
    
    public void productos() {
        this.escenario.productos();
    }
    
    public void compras() {
        this.escenario.compras();
    }
    
    public void formasPago() {
        this.escenario.formasPago();
    }
    
    public void clientes() {
        this.escenario.clientes();
    }
    
    public void usuarios() {
        this.escenario.usuarios();
    }
    
    public void ventas() {
        this.escenario.ventas();
    }
    
    public void facturas() {
        this.escenario.facturas();
    }

}
