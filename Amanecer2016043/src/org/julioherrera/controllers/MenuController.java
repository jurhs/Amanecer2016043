package org.julioherrera.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import org.julioherrera.beans.Usuario;
import org.julioherrera.system.Main;

public class MenuController implements Initializable {
    
    private Main escenario;
    private Usuario usuario;
    @FXML private Label lblUsuario;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }

    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }

    public void proveedores() {
        this.escenario.proveedores();
    }
    
    public void tiposProducto() {
        this.escenario.tiposProducto();
    }
    
    public void productos() {
        this.escenario.productos();
    }
    
    public void compras() {
        this.escenario.compras();
    }
    
    public void formasPago() {
        this.escenario.formasPago();
    }
    
    public void clientes() {
        this.escenario.clientes();
    }
    
    public void usuarios() {
        this.escenario.usuarios();
    }
    
    public void ventas() {
        this.escenario.ventas();
    }
    
    public void facturas() {
        this.escenario.facturas();
    }
    
    public void login() {
        this.escenario.login();
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
        lblUsuario.setText("Bienvenido " + usuario.getNombresUsuario() + " " + usuario.getApellidosUsuario());
    }
    
}
