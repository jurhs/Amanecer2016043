package org.julioherrera.controllers;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javax.swing.JOptionPane;
import org.julioherrera.beans.Proveedor;
import org.julioherrera.db.Conexion;
import org.julioherrera.system.Main;

public class ProveedoresController implements Initializable {
    
    private Main escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<Proveedor> listaProveedores;
    private boolean camposCorrectos;
    @FXML private TextField txtRazonSocial;
    @FXML private TextField txtNit;
    @FXML private TextField txtDireccionProveedor;
    @FXML private TextField txtPaginaWeb;
    @FXML private TableView tblProveedores;
    @FXML private TableColumn colCodigoProveedor;
    @FXML private TableColumn colRazonSocial;
    @FXML private TableColumn colNit;
    @FXML private TableColumn colDireccionProveedor;
    @FXML private TableColumn colPaginaWeb;
    @FXML private Circle btnAgregar;
    @FXML private Circle btnActualizar;
    @FXML private Circle btnEliminar;
    @FXML private ImageView imgAgregar;
    @FXML private ImageView imgActualizar;
    @FXML private ImageView imgEliminar;
    @FXML private ImageView imgGuardarAgregar;
    @FXML private ImageView imgGuardarActualizar;
    @FXML private ImageView imgCancelar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
        desactivarControles();
    }
    
    public void cargarDatos() {
        tblProveedores.setItems(getProveedores());
        colCodigoProveedor.setCellValueFactory(new PropertyValueFactory<Proveedor, Integer>("codigoProveedor"));
        colRazonSocial.setCellValueFactory(new PropertyValueFactory<Proveedor, String>("razonSocial"));
        colNit.setCellValueFactory(new PropertyValueFactory<Proveedor, String>("nit"));
        colDireccionProveedor.setCellValueFactory(new PropertyValueFactory<Proveedor, String>("direccionProveedor"));
        colPaginaWeb.setCellValueFactory(new PropertyValueFactory<Proveedor, String>("paginaWeb"));
    }
    
    public ObservableList<Proveedor> getProveedores() {
        ArrayList<Proveedor> lista = new ArrayList<Proveedor>();
        try {
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listProveedores}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Proveedor(resultado.getInt("codigoProveedor"), resultado.getString("razonSocial"), resultado.getString("nit"), resultado.getString("direccionProveedor"), resultado.getString("paginaWeb")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaProveedores = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos() {
        if(tblProveedores.getSelectionModel().getSelectedIndex() > -1) {
            Proveedor elemento = (Proveedor)tblProveedores.getSelectionModel().getSelectedItem();
            txtRazonSocial.setText(elemento.getRazonSocial());
            txtNit.setText(elemento.getNit());
            txtDireccionProveedor.setText(elemento.getDireccionProveedor());
            txtPaginaWeb.setText(elemento.getPaginaWeb());
        }
    }
    
    public void btnAgregar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                activarControles();
                limpiarControles();
                imgAgregar.setVisible(false);
                imgEliminar.setVisible(false);
                btnActualizar.setVisible(false);
                tipoOperaciones = operaciones.AGREGAR;
            break;
            case AGREGAR:
                agregar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgAgregar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnActualizar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnActualizar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblProveedores.getSelectionModel().getSelectedItem() != null) {
                    activarControles();
                    imgActualizar.setVisible(false);
                    imgEliminar.setVisible(false);
                    btnAgregar.setVisible(false);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Proveedor");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgActualizar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnAgregar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnEliminar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblProveedores.getSelectionModel().getSelectedItem() != null) {
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Proveedor",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION){
                        eliminar();
                        limpiarControles();
                        cargarDatos();
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Proveedor");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar() {
        try{
            if(!(txtRazonSocial.getText().equals(""))&&!(txtNit.getText().equals(""))&&!(txtDireccionProveedor.getText().equals(""))&&!(txtPaginaWeb.getText().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_insertProveedor(?,?,?,?)}");
                procedimiento.setString(1, (txtRazonSocial.getText())); //razonSocial
                procedimiento.setString(2, (txtNit.getText()));  //nit
                procedimiento.setString(3, (txtDireccionProveedor.getText())); //direccionProveedor
                procedimiento.setString(4, (txtPaginaWeb.getText())); //paginaWeb
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizar() {
        try{
            if(!(txtRazonSocial.getText().equals(""))&&!(txtNit.getText().equals(""))&&!(txtDireccionProveedor.getText().equals(""))&&!(txtPaginaWeb.getText().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_updateProveedor(?,?,?,?,?)}");
                procedimiento.setString(1, (txtRazonSocial.getText())); //razonSocial
                procedimiento.setString(2, (txtNit.getText()));  //nit
                procedimiento.setString(3, (txtDireccionProveedor.getText())); //direccionProveedor
                procedimiento.setString(4, (txtPaginaWeb.getText())); //paginaWeb
                procedimiento.setInt(5, (((Proveedor)tblProveedores.getSelectionModel().getSelectedItem()).getCodigoProveedor())); //codigoProveedor
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar() {
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_deleteProveedor(?)}");
            procedimiento.setInt(1, ((Proveedor)tblProveedores.getSelectionModel().getSelectedItem()).getCodigoProveedor());
            procedimiento.execute();
        }catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Un registro depende de este Proveedor");
        }
    }
    
    public void desactivarControles() {
        txtRazonSocial.setDisable(true);
        txtNit.setDisable(true);
        txtDireccionProveedor.setDisable(true);
        txtPaginaWeb.setDisable(true);
        imgGuardarAgregar.setVisible(false);
        imgGuardarActualizar.setVisible(false);
        imgCancelar.setVisible(false);
    }
    
    public void activarControles() {
        txtRazonSocial.setDisable(false);
        txtNit.setDisable(false);
        txtDireccionProveedor.setDisable(false);
        txtPaginaWeb.setDisable(false);
        imgGuardarAgregar.setVisible(true);
        imgGuardarActualizar.setVisible(true);
        imgCancelar.setVisible(true);
    }
    
    public void limpiarControles() {
        txtRazonSocial.setText("");
        txtNit.setText("");
        txtDireccionProveedor.setText("");
        txtPaginaWeb.setText("");
    }
    
    public void cancelar() {
        tblProveedores.getSelectionModel().clearSelection();
        desactivarControles();
        limpiarControles();
        cargarDatos();
        imgAgregar.setVisible(true);
        imgActualizar.setVisible(true);
        imgEliminar.setVisible(true);
        btnAgregar.setVisible(true);
        btnActualizar.setVisible(true);
        tipoOperaciones = operaciones.NINGUNO;
    }
    
    public void telefonosProveedor() {
        if(tblProveedores.getSelectionModel().getSelectedItem() != null) {
            this.escenario.telefonosProveedor(((Proveedor)tblProveedores.getSelectionModel().getSelectedItem()).getCodigoProveedor());
        }else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un Proveedor");
        }
    }

    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }
    
    public void menu() {
        this.escenario.menu(this.escenario.getUsuario());
    }
    
    public void tiposProducto() {
        this.escenario.tiposProducto();
    }
    
    public void productos() {
        this.escenario.productos();
    }
    
    public void compras() {
        this.escenario.compras();
    }
    
    public void formasPago() {
        this.escenario.formasPago();
    }
    
    public void clientes() {
        this.escenario.clientes();
    }
    
    public void usuarios() {
        this.escenario.usuarios();
    }
    
    public void ventas() {
        this.escenario.ventas();
    }
    
    public void facturas() {
        this.escenario.facturas();
    }
    
}
