package org.julioherrera.controllers;

import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javax.swing.JOptionPane;
import org.julioherrera.beans.TipoProducto;
import org.julioherrera.db.Conexion;
import org.julioherrera.system.Main;

public class TiposProductoController implements Initializable {
    
    private Main escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<TipoProducto> listaTiposProducto;
    private boolean camposCorrectos;
    @FXML private TextField txtDescripcion;
    @FXML private TableView tblTiposProducto;
    @FXML private TableColumn colCodigoTipoProducto;
    @FXML private TableColumn colDescripcion;
    @FXML private TableColumn colFechaCreacion;
    @FXML private TableColumn colFechaModificacion;
    @FXML private Circle btnAgregar;
    @FXML private Circle btnActualizar;
    @FXML private Circle btnEliminar;
    @FXML private ImageView imgAgregar;
    @FXML private ImageView imgActualizar;
    @FXML private ImageView imgEliminar;
    @FXML private ImageView imgGuardarAgregar;
    @FXML private ImageView imgGuardarActualizar;
    @FXML private ImageView imgCancelar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
        desactivarControles();
    }
    
    public void cargarDatos() {
        tblTiposProducto.setItems(getTiposProducto());
        colCodigoTipoProducto.setCellValueFactory(new PropertyValueFactory<TipoProducto, Integer>("codigoTipoProducto"));
        colDescripcion.setCellValueFactory(new PropertyValueFactory<TipoProducto, String>("descripcion"));
        colFechaCreacion.setCellValueFactory(new PropertyValueFactory<TipoProducto, Date>("fechaCreacion"));
        colFechaModificacion.setCellValueFactory(new PropertyValueFactory<TipoProducto, Date>("fechaModificacion"));
    }
    
    public ObservableList<TipoProducto> getTiposProducto() {
        ArrayList<TipoProducto> lista = new ArrayList<TipoProducto>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listTiposProducto}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new TipoProducto(resultado.getInt("codigoTipoProducto"), resultado.getString("descripcion"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaTiposProducto = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos() {
        if(tblTiposProducto.getSelectionModel().getSelectedIndex() > -1) {
            txtDescripcion.setText(((TipoProducto)tblTiposProducto.getSelectionModel().getSelectedItem()).getDescripcion());
        }
    }
    
    public void btnAgregar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                activarControles();
                limpiarControles();
                imgAgregar.setVisible(false);
                imgEliminar.setVisible(false);
                btnActualizar.setVisible(false);
                tipoOperaciones = operaciones.AGREGAR;
            break;
            case AGREGAR:
                agregar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgAgregar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnActualizar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnActualizar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblTiposProducto.getSelectionModel().getSelectedItem() != null) {
                    activarControles();
                    imgActualizar.setVisible(false);
                    imgEliminar.setVisible(false);
                    btnAgregar.setVisible(false);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Proveedor");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgActualizar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnAgregar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnEliminar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblTiposProducto.getSelectionModel().getSelectedItem() != null) {
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?", "Eliminar TipoProducto", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION) {
                        eliminar();
                        limpiarControles();
                        cargarDatos();
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Tipo de Producto");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar() {
        try {
            if(!(txtDescripcion.getText().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_insertTipoProducto(?,?,?)}");
                procedimiento.setString(1, (txtDescripcion.getText())); //Descripcion
                procedimiento.setDate(2, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())))); //fechaCreacion
                procedimiento.setDate(3, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())))); //fechaModificacion
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizar() {
        try{
            if(!(txtDescripcion.getText().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_updateTipoProducto(?,?,?)}");
                procedimiento.setString(1, (txtDescripcion.getText())); //descripcion
                procedimiento.setDate(2, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())))); //fechaModificacion
                procedimiento.setInt(3, (((TipoProducto)tblTiposProducto.getSelectionModel().getSelectedItem()).getCodigoTipoProducto())); //codigoTipoProducto
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar() {
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_deleteTipoProducto(?)}");
            procedimiento.setInt(1, ((TipoProducto)tblTiposProducto.getSelectionModel().getSelectedItem()).getCodigoTipoProducto());
            procedimiento.execute();
        }catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Un registro depende de este Tipo de Producto");
        }
    }
    
    public void desactivarControles() {
        txtDescripcion.setDisable(true);
        imgGuardarAgregar.setVisible(false);
        imgGuardarActualizar.setVisible(false);
        imgCancelar.setVisible(false);
    }
    
    public void activarControles() {
        txtDescripcion.setDisable(false);
        imgGuardarAgregar.setVisible(true);
        imgGuardarActualizar.setVisible(true);
        imgCancelar.setVisible(true);
    }
    
    public void limpiarControles() {
        txtDescripcion.setText("");
    }
    
    public void cancelar() {
        tblTiposProducto.getSelectionModel().clearSelection();
        desactivarControles();
        limpiarControles();
        cargarDatos();
        imgAgregar.setVisible(true);
        imgActualizar.setVisible(true);
        imgEliminar.setVisible(true);
        btnAgregar.setVisible(true);
        btnActualizar.setVisible(true);
        tipoOperaciones = operaciones.NINGUNO;
    }

    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }
    
    public void menu() {
        this.escenario.menu(this.escenario.getUsuario());
    }
    
    public void proveedores() {
        this.escenario.proveedores();
    }
    public void tiposProducto() {
        this.escenario.tiposProducto();
    }
    
    public void productos() {
        this.escenario.productos();
    }
    
    public void compras() {
        this.escenario.compras();
    }
    
    public void formasPago() {
        this.escenario.formasPago();
    }
    
    public void clientes() {
        this.escenario.clientes();
    }
    
    public void usuarios() {
        this.escenario.usuarios();
    }
    
    public void ventas() {
        this.escenario.ventas();
    }
    
    public void facturas() {
        this.escenario.facturas();
    }

}
