package org.julioherrera.controllers;

import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javax.swing.JOptionPane;
import org.julioherrera.beans.Producto;
import org.julioherrera.beans.Proveedor;
import org.julioherrera.beans.TipoProducto;
import org.julioherrera.db.Conexion;
import org.julioherrera.reports.GenerarReporte;
import org.julioherrera.system.Main;

public class ProductosController implements Initializable {
    
    private Main escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<Producto> listaProductos;
    private ObservableList<TipoProducto> listaTiposProducto;
    private ObservableList<Proveedor> listaProveedores;
    private boolean camposCorrectos;
    @FXML private TextField txtNombreProducto;
    @FXML private ComboBox cmbCodigoTipoProducto;
    @FXML private ComboBox cmbCodigoProveedor;
    @FXML private TableView tblProductos;
    @FXML private TableColumn colCodigoProducto;
    @FXML private TableColumn colNombreProducto;
    @FXML private TableColumn colExistencia;
    @FXML private TableColumn colPrecioCosto;
    @FXML private TableColumn colPrecioVenta;
    @FXML private TableColumn colProductoIva;
    @FXML private TableColumn colFechaCreacion;
    @FXML private TableColumn colFechaModificacion;
    @FXML private TableColumn colCodigoTipoProducto;
    @FXML private TableColumn colCodigoProveedor;
    @FXML private Circle btnAgregar;
    @FXML private Circle btnActualizar;
    @FXML private Circle btnEliminar;
    @FXML private ImageView imgAgregar;
    @FXML private ImageView imgActualizar;
    @FXML private ImageView imgEliminar;
    @FXML private ImageView imgGuardarAgregar;
    @FXML private ImageView imgGuardarActualizar;
    @FXML private ImageView imgCancelar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
        desactivarControles();
    }
    
    public void cargarDatos() {
        tblProductos.setItems(getProductos());
        colCodigoProducto.setCellValueFactory(new PropertyValueFactory<Producto, Integer>("codigoProducto"));
        colNombreProducto.setCellValueFactory(new PropertyValueFactory<Producto, String>("nombreProducto"));
        colExistencia.setCellValueFactory(new PropertyValueFactory<Producto, Integer>("existencia"));
        colPrecioCosto.setCellValueFactory(new PropertyValueFactory<Producto, Double>("precioCosto"));
        colPrecioVenta.setCellValueFactory(new PropertyValueFactory<Producto, Double>("precioVenta"));
        colProductoIva.setCellValueFactory(new PropertyValueFactory<Producto, Double>("productoIva"));
        colFechaCreacion.setCellValueFactory(new PropertyValueFactory<Producto, Date>("fechaCreacion"));
        colFechaModificacion.setCellValueFactory(new PropertyValueFactory<Producto, Date>("fechaModificacion"));
        colCodigoTipoProducto.setCellValueFactory(new PropertyValueFactory<Producto, Integer>("codigoTipoProducto"));
        colCodigoProveedor.setCellValueFactory(new PropertyValueFactory<Producto, Integer>("codigoProveedor"));
        cmbCodigoTipoProducto.setItems(getTiposProducto());
        cmbCodigoProveedor.setItems(getProveedores());
    }
    
    public ObservableList<Producto> getProductos() {
        ArrayList<Producto> lista = new ArrayList<Producto>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listProductos}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Producto(resultado.getInt("codigoProducto"), resultado.getString("nombreProducto"), resultado.getInt("existencia"), resultado.getDouble("precioCosto"), resultado.getDouble("precioVenta"), resultado.getDouble("productoIva"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion"), resultado.getInt("codigoTipoProducto"), resultado.getInt("codigoProveedor")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaProductos = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<TipoProducto> getTiposProducto() {
        ArrayList<TipoProducto> lista = new ArrayList<TipoProducto>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listTiposProducto}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new TipoProducto(resultado.getInt("codigoTipoProducto"), resultado.getString("descripcion"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaTiposProducto = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Proveedor> getProveedores() {
        ArrayList<Proveedor> lista = new ArrayList<Proveedor>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listProveedores}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Proveedor(resultado.getInt("codigoProveedor"), resultado.getString("razonSocial"), resultado.getString("nit"), resultado.getString("direccionProveedor"), resultado.getString("paginaWeb")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaProveedores = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos() {
        if(tblProductos.getSelectionModel().getSelectedIndex() > -1) {
            Producto elemento = (Producto)tblProductos.getSelectionModel().getSelectedItem();
            txtNombreProducto.setText(elemento.getNombreProducto());
            listaTiposProducto.stream().filter((tipoProducto) -> (tipoProducto.getCodigoTipoProducto() == elemento.getCodigoTipoProducto())).forEachOrdered((tipoProducto) -> {
                cmbCodigoTipoProducto.getSelectionModel().select(tipoProducto);
            });
            listaProveedores.stream().filter((proveedor) -> (proveedor.getCodigoProveedor() == elemento.getCodigoProveedor())).forEachOrdered((proveedor) -> {
                cmbCodigoProveedor.getSelectionModel().select(proveedor);
            });
        }
    }
    
    public void btnAgregar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                activarControles();
                limpiarControles();
                imgAgregar.setVisible(false);
                imgEliminar.setVisible(false);
                btnActualizar.setVisible(false);
                tipoOperaciones = operaciones.AGREGAR;
            break;
            case AGREGAR:
                agregar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgAgregar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnActualizar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnActualizar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblProductos.getSelectionModel().getSelectedItem() != null) {
                    activarControles();
                    imgActualizar.setVisible(false);
                    imgEliminar.setVisible(false);
                    btnAgregar.setVisible(false);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Producto");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgActualizar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnAgregar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnEliminar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblProductos.getSelectionModel().getSelectedItem() != null) {
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Proveedor",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION) {
                        eliminar();
                        limpiarControles();
                        cargarDatos();
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Producto");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar() {
        try{
            if(!(txtNombreProducto.getText().equals(""))&&!(cmbCodigoTipoProducto.getValue().equals(""))&&!(cmbCodigoProveedor.getValue().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_insertProducto(?,?,?,?,?,?,?,?,?)}");
                procedimiento.setString(1, txtNombreProducto.getText());
                procedimiento.setInt(2, 0); //existencia
                procedimiento.setDouble(3, 0); //precioCosto
                procedimiento.setDouble(4, 0); //precioVenta
                procedimiento.setDouble(5, 0); //productoIva
                procedimiento.setDate(6, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())))); //fechaCreacion
                procedimiento.setDate(7, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())))); //fechaModificacion
                procedimiento.setInt(8, (((TipoProducto)cmbCodigoTipoProducto.getSelectionModel().getSelectedItem()).getCodigoTipoProducto())); //codigoTipoProducto
                procedimiento.setInt(9, (((Proveedor)cmbCodigoProveedor.getSelectionModel().getSelectedItem()).getCodigoProveedor())); //codigoProveedor
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizar() {
        try{
            if(!(txtNombreProducto.getText().equals(""))&&!(cmbCodigoTipoProducto.getValue().equals(""))&&!(cmbCodigoProveedor.getValue().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_updateProducto(?,?,?,?,?)}");
                procedimiento.setString(1, txtNombreProducto.getText()); //nombreProducto
                procedimiento.setDate(2, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())))); //fechaModificacion
                procedimiento.setInt(3, (((TipoProducto)cmbCodigoTipoProducto.getSelectionModel().getSelectedItem()).getCodigoTipoProducto())); //codigoTipoProducto
                procedimiento.setInt(4, (((Proveedor)cmbCodigoProveedor.getSelectionModel().getSelectedItem()).getCodigoProveedor())); //codigoProveedor
                procedimiento.setInt(5, (((Producto)tblProductos.getSelectionModel().getSelectedItem()).getCodigoProducto())); //codigoProducto
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar() {
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_deleteProducto(?)}");
            procedimiento.setInt(1, (((Producto)tblProductos.getSelectionModel().getSelectedItem()).getCodigoProducto()));
            procedimiento.execute();
        }catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Un registro depende de este Producto");
        }
    }
    
    public void desactivarControles() {
        txtNombreProducto.setDisable(true);
        cmbCodigoTipoProducto.setDisable(true);
        cmbCodigoProveedor.setDisable(true);
        imgGuardarAgregar.setVisible(false);
        imgGuardarActualizar.setVisible(false);
        imgCancelar.setVisible(false);
    }

    public void activarControles() {
        txtNombreProducto.setDisable(false);
        cmbCodigoTipoProducto.setDisable(false);
        cmbCodigoProveedor.setDisable(false);
        imgGuardarAgregar.setVisible(true);
        imgGuardarActualizar.setVisible(true);
        imgCancelar.setVisible(true);
    }
    
    public void limpiarControles() {
        txtNombreProducto.setText("");
        cmbCodigoTipoProducto.setValue("");
        cmbCodigoProveedor.setValue("");
    }
    
    public void cancelar() {
        tblProductos.getSelectionModel().clearSelection();
        desactivarControles();
        limpiarControles();
        cargarDatos();
        imgAgregar.setVisible(true);
        imgActualizar.setVisible(true);
        imgEliminar.setVisible(true);
        btnAgregar.setVisible(true);
        btnActualizar.setVisible(true);
        tipoOperaciones = operaciones.NINGUNO;
    }
    
    public void generarReporte(){
        int respuesta = JOptionPane.showConfirmDialog(null, "¿Desea ver el Reporte de todos los productos?","Ver reporte",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
        if(respuesta == JOptionPane.YES_OPTION){
            Map parametros = new HashMap();
            int codProducto = 0;
            parametros.put("", codProducto);
            GenerarReporte.mostrarReporte("Productos.jasper", "Reporte de Productos", parametros);
        }
    }
    
    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }
    
    public void menu() {
        this.escenario.menu(this.escenario.getUsuario());
    }
    
    public void proveedores() {
        this.escenario.proveedores();
    }
    
    public void tiposProducto() {
        this.escenario.tiposProducto();
    }
    
    public void productos() {
        this.escenario.productos();
    }
    
    public void compras() {
        this.escenario.compras();
    }
    
    public void formasPago() {
        this.escenario.formasPago();
    }
    
    public void clientes() {
        this.escenario.clientes();
    }
    
    public void usuarios() {
        this.escenario.usuarios();
    }
    
    public void ventas() {
        this.escenario.ventas();
    }
    
    public void facturas() {
        this.escenario.facturas();
    }
    
}
