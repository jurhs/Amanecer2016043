package org.julioherrera.controllers;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javax.swing.JOptionPane;
import org.julioherrera.beans.Compra;
import org.julioherrera.beans.Producto;
import org.julioherrera.beans.Proveedor;
import org.julioherrera.db.Conexion;
import org.julioherrera.system.Main;

public class ComprasController implements Initializable {

    private Main escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<Compra> listaCompras;
    private ObservableList<Proveedor> listaProveedores;
    private ObservableList<Producto> listaProductos;
    private boolean camposCorrectos;
    @FXML private TextField txtDescripcion;
    @FXML private TextField txtPrecioUnitario;
    @FXML private TextField txtCantidad;
    @FXML private ComboBox cmbCodigoProveedor;
    @FXML private ComboBox cmbCodigoProducto;
    @FXML private TableView tblCompras;
    @FXML private TableColumn colCodigoCompra;
    @FXML private TableColumn colDescripcion;
    @FXML private TableColumn colPrecioUnitario;
    @FXML private TableColumn colCantidad;
    @FXML private TableColumn colTotal;
    @FXML private TableColumn colCodigoProveedor;
    @FXML private TableColumn colCodigoProducto;
    @FXML private Circle btnAgregar;
    @FXML private Circle btnActualizar;
    @FXML private Circle btnEliminar;
    @FXML private ImageView imgAgregar;
    @FXML private ImageView imgActualizar;
    @FXML private ImageView imgEliminar;
    @FXML private ImageView imgGuardarAgregar;
    @FXML private ImageView imgGuardarActualizar;
    @FXML private ImageView imgCancelar;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
        desactivarControles();
    }
    
    public void cargarDatos() {
        tblCompras.setItems(getCompras());
        colCodigoCompra.setCellValueFactory(new PropertyValueFactory<Compra, Integer>("codigoCompra"));
        colDescripcion.setCellValueFactory(new PropertyValueFactory<Compra, String>("descripcion"));
        colPrecioUnitario.setCellValueFactory(new PropertyValueFactory<Compra, Double>("precioUnitario"));
        colCantidad.setCellValueFactory(new PropertyValueFactory<Compra, Integer>("cantidad"));
        colTotal.setCellValueFactory(new PropertyValueFactory<Compra, Double>("total"));
        colCodigoProveedor.setCellValueFactory(new PropertyValueFactory<Compra, Integer>("codigoProveedor"));
        colCodigoProducto.setCellValueFactory(new PropertyValueFactory<Compra, Integer>("codigoProducto"));
        cmbCodigoProveedor.setItems(getProveedores());
        cmbCodigoProducto.setItems(getProductos());
    }
    
    public ObservableList<Compra> getCompras() {
        ArrayList<Compra> lista = new ArrayList<Compra>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listCompras}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Compra(resultado.getInt("codigoCompra"), resultado.getString("descripcion"), resultado.getDouble("precioUnitario"), resultado.getInt("cantidad"), resultado.getDouble("total"), resultado.getInt("codigoProveedor"), resultado.getInt("codigoProducto")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaCompras = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Proveedor> getProveedores() {
        ArrayList<Proveedor> lista = new ArrayList<Proveedor>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listProveedores}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Proveedor(resultado.getInt("codigoProveedor"), resultado.getString("razonSocial"), resultado.getString("nit"), resultado.getString("direccionProveedor"), resultado.getString("paginaWeb")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaProveedores = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Producto> getProductos() {
        ArrayList<Producto> lista = new ArrayList<Producto>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listProductos}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Producto(resultado.getInt("codigoProducto"), resultado.getString("nombreProducto"), resultado.getInt("existencia"), resultado.getDouble("precioCosto"), resultado.getDouble("precioVenta"), resultado.getDouble("productoIva"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion"), resultado.getInt("codigoTipoProducto"), resultado.getInt("codigoProveedor")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaProductos = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos() {
        if(tblCompras.getSelectionModel().getSelectedIndex() > -1) {
            Compra elemento = (Compra)tblCompras.getSelectionModel().getSelectedItem();
            txtDescripcion.setText(elemento.getDescripcion());
            txtPrecioUnitario.setText(String.valueOf(elemento.getPrecioUnitario()));
            txtCantidad.setText(String.valueOf(elemento.getCantidad()));
            listaProveedores.stream().filter((proveedor) -> (proveedor.getCodigoProveedor() == elemento.getCodigoProveedor())).forEachOrdered((proveedor) -> {
                cmbCodigoProveedor.getSelectionModel().select(proveedor);
            });
            listaProductos.stream().filter((producto) -> (producto.getCodigoProducto() == elemento.getCodigoProducto())).forEachOrdered((producto) -> {
                cmbCodigoProducto.getSelectionModel().select(producto);
            });
        }
    }
    
    public void btnAgregar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                activarControles();
                limpiarControles();
                imgAgregar.setVisible(false);
                imgEliminar.setVisible(false);
                btnActualizar.setVisible(false);
                tipoOperaciones = operaciones.AGREGAR;
            break;
            case AGREGAR:
                agregar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgAgregar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnActualizar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnActualizar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblCompras.getSelectionModel().getSelectedItem() != null) {
                    activarControles();
                    imgActualizar.setVisible(false);
                    imgEliminar.setVisible(false);
                    btnAgregar.setVisible(false);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar una Compra");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgActualizar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnAgregar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnEliminar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblCompras.getSelectionModel().getSelectedItem() != null) {
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Compra",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION) {
                        eliminar();
                        limpiarControles();
                        cargarDatos();
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar una Compra");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar() {
        boolean entrar = true;
        Double precioUnitario = null;
        int cantidad = 0;
        try{
            precioUnitario = Double.parseDouble((txtPrecioUnitario.getText()));
            cantidad = Integer.parseInt((txtCantidad.getText()));
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Los campos 'Precio Unitario' y 'Cantidad' deben de ser números");
            entrar = false;
            camposCorrectos = false;
        }
        try{
            if(!(txtDescripcion.getText().equals(""))&&!(txtPrecioUnitario.getText().equals(""))&&!(txtCantidad.getText().equals(""))&&!(cmbCodigoProveedor.getValue().equals(""))&&!(cmbCodigoProducto.getValue().equals(""))&&(entrar)) {
                do {
                    PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_insertCompra(?,?,?,?,?,?)}");
                    procedimiento.setString(1, (txtDescripcion.getText())); //descripcion
                    procedimiento.setDouble(2, Double.parseDouble(txtPrecioUnitario.getText())); //precioUnitario
                    procedimiento.setInt(3, Integer.parseInt(txtCantidad.getText())); //cantidad
                    procedimiento.setDouble(4, (Double.parseDouble(txtPrecioUnitario.getText()) * Integer.parseInt(txtCantidad.getText()))); //total
                    procedimiento.setInt(5, (((Proveedor)cmbCodigoProveedor.getSelectionModel().getSelectedItem()).getCodigoProveedor())); //codigoProveedor
                    procedimiento.setInt(6, (((Producto)cmbCodigoProducto.getSelectionModel().getSelectedItem()).getCodigoProducto())); //codigoProducto
                    if(precioUnitario * cantidad <= 0) {
                        JOptionPane.showMessageDialog(null, "El total de compra no puede ser 0 o negativo!");
                    }
                    if((precioUnitario * cantidad) > 0) {
                        procedimiento.execute();
                        camposCorrectos = true;
                        cantidad = 0;
                    }
                }while((precioUnitario * cantidad) > 0);
            }else {
                if(entrar) {
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    camposCorrectos = false;
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizar() {
        boolean entrar = true;
        Double precioUnitario = null;
        int cantidad = 0;
        try{
            precioUnitario = Double.parseDouble((txtPrecioUnitario.getText()));
            cantidad = Integer.parseInt((txtCantidad.getText()));
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Los campos 'Precio Unitario' y 'Cantidad' deben de ser números");
            entrar = false;
            camposCorrectos = false;
        }
        try{
            if(!(txtDescripcion.getText().equals(""))&&!(txtPrecioUnitario.getText().equals(""))&&!(txtCantidad.getText().equals(""))&&!(cmbCodigoProveedor.getValue().equals(""))&&!(cmbCodigoProducto.getValue().equals(""))&&(entrar)) {
                do {
                    PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_updateCompra(?,?,?,?,?,?,?)}");
                    procedimiento.setString(1, (txtDescripcion.getText())); //descripcion
                    procedimiento.setDouble(2, Double.parseDouble(txtPrecioUnitario.getText())); //precioUnitario
                    procedimiento.setInt(3, Integer.parseInt(txtCantidad.getText())); //cantidad
                    procedimiento.setDouble(4, (Double.parseDouble(txtPrecioUnitario.getText()) * Integer.parseInt(txtCantidad.getText()))); //total
                    procedimiento.setInt(5, (((Proveedor)cmbCodigoProveedor.getSelectionModel().getSelectedItem()).getCodigoProveedor())); //codigoProveedor
                    procedimiento.setInt(6, (((Producto)cmbCodigoProducto.getSelectionModel().getSelectedItem()).getCodigoProducto())); //codigoProducto
                    procedimiento.setInt(7, (((Compra)tblCompras.getSelectionModel().getSelectedItem()).getCodigoCompra())); //codigoCompra
                    if(precioUnitario * cantidad <= 0) {
                        JOptionPane.showMessageDialog(null, "El total de compra no puede ser 0 o negativo!");
                    }
                    if((precioUnitario * cantidad) > 0) {
                        procedimiento.execute();
                        camposCorrectos = true;
                        cantidad = 0;
                    }
                }while((precioUnitario * cantidad) > 0);
            }else {
                if(entrar) {
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    camposCorrectos = false;
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar() {
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_deleteCompra(?)}");
            procedimiento.setInt(1, (((Compra)tblCompras.getSelectionModel().getSelectedItem()).getCodigoCompra()));
            procedimiento.execute();
        }catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Un registro depende de esta Compra");
        }
    }
    
    public void desactivarControles() {
        txtDescripcion.setDisable(true);
        txtPrecioUnitario.setDisable(true);
        txtCantidad.setDisable(true);
        cmbCodigoProveedor.setDisable(true);
        cmbCodigoProducto.setDisable(true);
        imgGuardarAgregar.setVisible(false);
        imgGuardarActualizar.setVisible(false);
        imgCancelar.setVisible(false);
    }
    
    public void activarControles() {
        txtDescripcion.setDisable(false);
        txtPrecioUnitario.setDisable(false);
        txtCantidad.setDisable(false);
        cmbCodigoProveedor.setDisable(false);
        cmbCodigoProducto.setDisable(false);
        imgGuardarAgregar.setVisible(true);
        imgGuardarActualizar.setVisible(true);
        imgCancelar.setVisible(true);
    }
    
    public void limpiarControles() {
        txtDescripcion.setText("");
        txtPrecioUnitario.setText("0.00");
        txtCantidad.setText("0");
        cmbCodigoProveedor.setValue("");
        cmbCodigoProducto.setValue("");
    }
    
    public void cancelar() {
        tblCompras.getSelectionModel().clearSelection();
        desactivarControles();
        limpiarControles();
        cargarDatos();
        imgAgregar.setVisible(true);
        imgActualizar.setVisible(true);
        imgEliminar.setVisible(true);
        btnAgregar.setVisible(true);
        btnActualizar.setVisible(true);
        tipoOperaciones = operaciones.NINGUNO;
    }

    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }
    
    public void menu() {
        this.escenario.menu(this.escenario.getUsuario());
    }
    
    public void proveedores() {
        this.escenario.proveedores();
    }
    
    public void tiposProducto() {
        this.escenario.tiposProducto();
    }
    
    public void productos() {
        this.escenario.productos();
    }
    
    public void compras() {
        this.escenario.compras();
    }
    
    public void formasPago() {
        this.escenario.formasPago();
    }
    
    public void clientes() {
        this.escenario.clientes();
    }
    
    public void usuarios() {
        this.escenario.usuarios();
    }
    
    public void ventas() {
        this.escenario.ventas();
    }
    
    public void facturas() {
        this.escenario.facturas();
    }

}
