package org.julioherrera.controllers;

import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javax.swing.JOptionPane;
import org.julioherrera.beans.Cliente;
import org.julioherrera.db.Conexion;
import org.julioherrera.system.Main;

public class ClientesController implements Initializable {
    
    private Main escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones tipoOperaciones = operaciones.NINGUNO;
    private ObservableList<Cliente> listaClientes;
    private boolean camposCorrectos;
    @FXML private TextField txtNombresCliente;
    @FXML private TextField txtApellidosCliente;
    @FXML private TextField txtDireccionCliente;
    @FXML private TextField txtNitCliente;
    @FXML private TableView tblClientes;
    @FXML private TableColumn colCodigoCliente;
    @FXML private TableColumn colNombresCliente;
    @FXML private TableColumn colApellidosCliente;
    @FXML private TableColumn colDireccionCliente;
    @FXML private TableColumn colNitCliente;
    @FXML private TableColumn colFechaCreacion;
    @FXML private TableColumn colFechaModificacion;
    @FXML private Circle btnAgregar;
    @FXML private Circle btnActualizar;
    @FXML private Circle btnEliminar;
    @FXML private ImageView imgAgregar;
    @FXML private ImageView imgActualizar;
    @FXML private ImageView imgEliminar;
    @FXML private ImageView imgGuardarAgregar;
    @FXML private ImageView imgGuardarActualizar;
    @FXML private ImageView imgCancelar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
        desactivarControles();
    }
    
    public void cargarDatos() {
        tblClientes.setItems(getClientes());
        colCodigoCliente.setCellValueFactory(new PropertyValueFactory<Cliente, Integer>("codigoCliente"));
        colNombresCliente.setCellValueFactory(new PropertyValueFactory<Cliente, String>("nombresCliente"));
        colApellidosCliente.setCellValueFactory(new PropertyValueFactory<Cliente, String>("apellidosCliente"));
        colDireccionCliente.setCellValueFactory(new PropertyValueFactory<Cliente, String>("direccionCliente"));
        colNitCliente.setCellValueFactory(new PropertyValueFactory<Cliente, String>("nitCliente"));
        colFechaCreacion.setCellValueFactory(new PropertyValueFactory<Cliente, Date>("fechaCreacion"));
        colFechaModificacion.setCellValueFactory(new PropertyValueFactory<Cliente, Date>("fechaModificacion"));
    }
    
    public ObservableList<Cliente> getClientes() {
        ArrayList<Cliente> lista = new ArrayList<Cliente>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listClientes}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Cliente(resultado.getInt("codigoCliente"), resultado.getString("nombresCliente"), resultado.getString("apellidosCliente"), resultado.getString("direccionCliente"), resultado.getString("nitCliente"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaClientes = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos() {
        if(tblClientes.getSelectionModel().getSelectedIndex() > -1) {
            Cliente elemento = ((Cliente)tblClientes.getSelectionModel().getSelectedItem());
            txtNombresCliente.setText(elemento.getNombresCliente());
            txtApellidosCliente.setText(elemento.getApellidosCliente());
            txtDireccionCliente.setText(elemento.getDireccionCliente());
            txtNitCliente.setText(elemento.getNitCliente());
        }
    }
    
    public void btnAgregar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                activarControles();
                limpiarControles();
                imgAgregar.setVisible(false);
                imgEliminar.setVisible(false);
                btnActualizar.setVisible(false);
                tipoOperaciones = operaciones.AGREGAR;
            break;
            case AGREGAR:
                agregar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgAgregar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnActualizar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnActualizar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblClientes.getSelectionModel().getSelectedItem() != null) {
                    activarControles();
                    imgActualizar.setVisible(false);
                    imgEliminar.setVisible(false);
                    btnAgregar.setVisible(false);
                    tipoOperaciones = operaciones.ACTUALIZAR;
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Cliente");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgActualizar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnAgregar.setVisible(true);
                    tipoOperaciones = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnEliminar() {
        switch(tipoOperaciones) {
            case NINGUNO:
                if(tblClientes.getSelectionModel().getSelectedItem() != null) {
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?", "Eliminar Cliente", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION) {
                        eliminar();
                        limpiarControles();
                        cargarDatos();
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Cliente");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar() {
        try{
            if(!(txtNombresCliente.getText().equals(""))&&!(txtApellidosCliente.getText().equals(""))&&!(txtDireccionCliente.getText().equals(""))&&!(txtNitCliente.getText().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_insertCliente(?,?,?,?,?,?)}");
                procedimiento.setString(1, (txtNombresCliente.getText())); //nombresCliente
                procedimiento.setString(2, (txtApellidosCliente.getText())); //apellidosCliente
                procedimiento.setString(3, (txtDireccionCliente.getText())); //direccionCliente
                procedimiento.setString(4, (txtNitCliente.getText())); //nitCliente
                procedimiento.setDate(5, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())))); //fechaCreacion
                procedimiento.setDate(6, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())))); //fechaModificacion
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizar() {
        try{
            if(!(txtNombresCliente.getText().equals(""))&&!(txtApellidosCliente.getText().equals(""))&&!(txtDireccionCliente.getText().equals(""))&&!(txtNitCliente.getText().equals(""))) {
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_updateCliente(?,?,?,?,?,?)}");
                procedimiento.setString(1, (txtNombresCliente.getText())); //nombresCliente
                procedimiento.setString(2, (txtApellidosCliente.getText())); //apellidosCliente
                procedimiento.setString(3, (txtDireccionCliente.getText())); //direccionCliente
                procedimiento.setString(4, (txtNitCliente.getText())); //nitCliente
                procedimiento.setDate(5, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())))); //fechaModificacion
                procedimiento.setInt(6, (((Cliente)tblClientes.getSelectionModel().getSelectedItem()).getCodigoCliente())); //codigoCliente
                procedimiento.execute();
                camposCorrectos = true;
            }else {
                JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                camposCorrectos = false;
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar() {
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_deleteCliente(?)}");
            procedimiento.setInt(1, (((Cliente)tblClientes.getSelectionModel().getSelectedItem()).getCodigoCliente())); //codigoCliente
            procedimiento.execute();
        }catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Un registro depende de este Cliente");
        }
    }
    
    public void desactivarControles() {
        txtNombresCliente.setDisable(true);
        txtApellidosCliente.setDisable(true);
        txtDireccionCliente.setDisable(true);
        txtNitCliente.setDisable(true);
        imgGuardarAgregar.setVisible(false);
        imgGuardarActualizar.setVisible(false);
        imgCancelar.setVisible(false);
    }
    
    public void activarControles() {
        txtNombresCliente.setDisable(false);
        txtApellidosCliente.setDisable(false);
        txtDireccionCliente.setDisable(false);
        txtNitCliente.setDisable(false);
        imgGuardarAgregar.setVisible(true);
        imgGuardarActualizar.setVisible(true);
        imgCancelar.setVisible(true);
    }
    
    public void limpiarControles() {
        txtNombresCliente.setText("");
        txtApellidosCliente.setText("");
        txtDireccionCliente.setText("");
        txtNitCliente.setText("");
    }
    
    public void cancelar() {
        tblClientes.getSelectionModel().clearSelection();
        desactivarControles();
        limpiarControles();
        cargarDatos();
        imgAgregar.setVisible(true);
        imgActualizar.setVisible(true);
        imgEliminar.setVisible(true);
        btnAgregar.setVisible(true);
        btnActualizar.setVisible(true);
        tipoOperaciones = operaciones.NINGUNO;
    }
    
    public void telefonosCliente() {
        if(tblClientes.getSelectionModel().getSelectedItem() != null) {
            this.escenario.telefonosCliente(((Cliente)tblClientes.getSelectionModel().getSelectedItem()).getCodigoCliente());
        }else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un Cliente");
        }
    }
    
    public void emailsCliente() {
        if(tblClientes.getSelectionModel().getSelectedItem() != null) {
            this.escenario.emailsCliente(((Cliente)tblClientes.getSelectionModel().getSelectedItem()).getCodigoCliente());
        }else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un Cliente");
        }
    }

    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }
    
    public void menu() {
        this.escenario.menu(this.escenario.getUsuario());
    }
    
    public void proveedores() {
        this.escenario.proveedores();
    }
    
    public void tiposProducto() {
        this.escenario.tiposProducto();
    }
    
    public void productos() {
        this.escenario.productos();
    }
    
    public void compras() {
        this.escenario.compras();
    }
    
    public void formasPago() {
        this.escenario.formasPago();
    }
    
    public void clientes() {
        this.escenario.clientes();
    }
    
    public void usuarios() {
        this.escenario.usuarios();
    }
    
    public void ventas() {
        this.escenario.ventas();
    }
    
    public void facturas() {
        this.escenario.facturas();
    }

}
