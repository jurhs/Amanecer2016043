package org.julioherrera.controllers;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javafx.util.StringConverter;
import javax.swing.JOptionPane;
import org.julioherrera.beans.Cliente;
import org.julioherrera.beans.FormaPago;
import org.julioherrera.beans.TipoProducto;
import org.julioherrera.beans.Usuario;
import org.julioherrera.beans.Venta;
import org.julioherrera.db.Conexion;
import org.julioherrera.reports.GenerarReporte;
import org.julioherrera.system.Main;

public class VentasController implements Initializable {
    
    private Main escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones operacionActual = operaciones.NINGUNO;
    private ObservableList<Venta> listaVentas;
    private ObservableList<Cliente> listaClientes;
    private ObservableList<FormaPago> listaFormasPago;
    private ObservableList<TipoProducto> listaTiposProducto;
    private ObservableList<Usuario> listaUsuarios;
    private boolean camposCorrectos;
    @FXML private JFXDatePicker dtpFechaVenta;
    @FXML private JFXTextField txtObservacion;
    @FXML private JFXComboBox cmbCodigoCliente;
    @FXML private JFXComboBox cmbCodigoFormaPago;
    @FXML private JFXComboBox cmbCodigoTipoProducto;
    @FXML private JFXComboBox cmbCodigoUsuario;
    @FXML private JFXTextField txtNumeroFactura;
    @FXML private TableView tblVentas;
    @FXML private TableColumn colCodigoVenta;
    @FXML private TableColumn colFechaVenta;
    @FXML private TableColumn colObservacion;
    @FXML private TableColumn colFechaCreacion;
    @FXML private TableColumn colFechaModificacion;
    @FXML private TableColumn colTotal;
    @FXML private TableColumn colNumeroFactura;
    @FXML private TableColumn colCodigoCliente;
    @FXML private TableColumn colCodigoFormaPago;
    @FXML private TableColumn colCodigoTipoProducto;
    @FXML private TableColumn colCodigoUsuario;
    @FXML private Circle btnAgregar;
    @FXML private Circle btnActualizar;
    @FXML private Circle btnEliminar;
    @FXML private ImageView imgAgregar;
    @FXML private ImageView imgActualizar;
    @FXML private ImageView imgEliminar;
    @FXML private ImageView imgGuardarAgregar;
    @FXML private ImageView imgGuardarActualizar;
    @FXML private ImageView imgCancelar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
        desactivarControles();
        datePickerFormater();
    }
    
    public void cargarDatos() {
        tblVentas.setItems(getVentas());
        colCodigoVenta.setCellValueFactory(new PropertyValueFactory<Venta, Integer>("codigoVenta"));
        colFechaVenta.setCellValueFactory(new PropertyValueFactory<Venta, Date>("fechaVenta"));
        colObservacion.setCellValueFactory(new PropertyValueFactory<Venta, String>("observacion"));
        colFechaCreacion.setCellValueFactory(new PropertyValueFactory<Venta, Date>("fechaCreacion"));
        colFechaModificacion.setCellValueFactory(new PropertyValueFactory<Venta, Date>("fechaModificacion"));
        colTotal.setCellValueFactory(new PropertyValueFactory<Venta, Double>("total"));
        colNumeroFactura.setCellValueFactory(new PropertyValueFactory<Venta, Integer>("numeroFactura"));
        colCodigoCliente.setCellValueFactory(new PropertyValueFactory<Venta, Integer>("codigoCliente"));
        colCodigoFormaPago.setCellValueFactory(new PropertyValueFactory<Venta, Integer>("codigoFormaPago"));
        colCodigoTipoProducto.setCellValueFactory(new PropertyValueFactory<Venta, Integer>("codigoTipoProducto"));
        colCodigoUsuario.setCellValueFactory(new PropertyValueFactory<Venta, Integer>("codigoUsuario"));
        cmbCodigoCliente.setItems(getClientes());
        cmbCodigoFormaPago.setItems(getFormasPago());
        cmbCodigoTipoProducto.setItems(getTiposProducto());
        cmbCodigoUsuario.setItems(getUsuarios());
    }
    
    public ObservableList<Venta> getVentas() {
        ArrayList<Venta> lista = new ArrayList<Venta>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listVentas}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Venta(resultado.getInt("codigoVenta"), resultado.getDate("fechaVenta"), resultado.getInt("iva"), resultado.getString("observacion"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion"), resultado.getDouble("total"), resultado.getInt("numeroFactura"), resultado.getInt("codigoCliente"), resultado.getInt("codigoFormaPago"), resultado.getInt("codigoTipoProducto"), resultado.getInt("codigoUsuario")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaVentas = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Cliente> getClientes() {
        ArrayList<Cliente> lista = new ArrayList<Cliente>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listClientes}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Cliente(resultado.getInt("codigoCliente"), resultado.getString("nombresCliente"), resultado.getString("apellidosCliente"), resultado.getString("direccionCliente"), resultado.getString("nitCliente"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaClientes = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<FormaPago> getFormasPago() {
        ArrayList<FormaPago> lista = new ArrayList<FormaPago>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listFormasPago}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new FormaPago(resultado.getInt("codigoFormaPago"), resultado.getString("descripcion")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaFormasPago = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<TipoProducto> getTiposProducto() {
        ArrayList<TipoProducto> lista = new ArrayList<TipoProducto>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listTiposProducto}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new TipoProducto(resultado.getInt("codigoTipoProducto"), resultado.getString("descripcion"), resultado.getDate("fechaCreacion"), resultado.getDate("fechaModificacion")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaTiposProducto = FXCollections.observableArrayList(lista);
    }
    
    public ObservableList<Usuario> getUsuarios() {
        ArrayList<Usuario> lista = new ArrayList<Usuario>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listUsuarios}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new Usuario(resultado.getInt("codigoUsuario"), resultado.getString("nombresUsuario"), resultado.getString("apellidosUsuario"), resultado.getString("usuario"), resultado.getString("contrasena")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaUsuarios = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos() {
        if(tblVentas.getSelectionModel().getSelectedIndex() > -1) {
            Venta elemento = (Venta)tblVentas.getSelectionModel().getSelectedItem();
            dtpFechaVenta.setValue(LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(elemento.getFechaVenta())));
            txtObservacion.setText(elemento.getObservacion());
            listaClientes.stream().filter((cliente) -> (cliente.getCodigoCliente() == elemento.getCodigoCliente())).forEachOrdered((cliente) -> {
                cmbCodigoCliente.setValue(cliente);
            });
            listaFormasPago.stream().filter((formaPago) -> (formaPago.getCodigoFormaPago() == elemento.getCodigoFormaPago())).forEachOrdered((formaPago) -> {
                cmbCodigoFormaPago.setValue(formaPago);
            });
            listaTiposProducto.stream().filter((tipoProducto) -> (tipoProducto.getCodigoTipoProducto() == elemento.getCodigoTipoProducto())).forEachOrdered((tipoProducto) -> {
                cmbCodigoTipoProducto.setValue(tipoProducto);
            });
            listaUsuarios.stream().filter((usuario) -> (usuario.getCodigoUsuario() == elemento.getCodigoUsuario())).forEachOrdered((usuario) -> {
                cmbCodigoUsuario.setValue(usuario);
            });
            txtNumeroFactura.setText(String.valueOf(elemento.getNumeroFactura()));
        }
    }
    
    public void btnAgregar() {
        switch(operacionActual) {
            case NINGUNO:
                activarControles();
                limpiarControles();
                imgAgregar.setVisible(false);
                imgEliminar.setVisible(false);
                btnActualizar.setVisible(false);
                operacionActual = operaciones.AGREGAR;
            break;
            case AGREGAR:
                agregar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgAgregar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnActualizar.setVisible(true);
                    operacionActual = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnActualizar() {
        switch(operacionActual) {
            case NINGUNO:
                if(tblVentas.getSelectionModel().getSelectedItem() != null) {
                    activarControles();
                    imgActualizar.setVisible(false);
                    imgEliminar.setVisible(false);
                    btnAgregar.setVisible(false);
                    operacionActual = operaciones.ACTUALIZAR;
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar una Venta");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgActualizar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnAgregar.setVisible(true);
                    operacionActual = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnEliminar() {
        switch(operacionActual) {
            case NINGUNO:
                if(tblVentas.getSelectionModel().getSelectedItem() != null) {
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?", "Eliminar Venta", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION) {
                        eliminar();
                        limpiarControles();
                        cargarDatos();
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar una Venta");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar() {
        boolean entrar = true;
        int numeroFactura = 0;
        try{
            numeroFactura = Integer.parseInt((txtNumeroFactura.getText()));
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "El campo 'Número de Factura' debe de ser un número");
            entrar = false;
            camposCorrectos = false;
        }
        try{
            if(!(txtObservacion.getText().equals(""))&&!(txtNumeroFactura.getText().equals(""))&&!(cmbCodigoCliente.getValue().equals(""))&&!(cmbCodigoFormaPago.getValue().equals(""))&&!(cmbCodigoTipoProducto.getValue().equals(""))&&!(cmbCodigoUsuario.getValue().equals(""))&&(entrar)) {
                do{
                    PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_insertVenta(?,?,?,?,?,?,?,?,?,?,?)}");
                    procedimiento.setDate(1, (Date.valueOf(dtpFechaVenta.getValue()))); //fechaVenta
                    procedimiento.setInt(2, 12); //iva
                    procedimiento.setString(3, (txtObservacion.getText())); //observacion
                    procedimiento.setDate(4, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())))); //fechaCreacion
                    procedimiento.setDate(5, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())))); //fechaModificacion
                    procedimiento.setDouble(6, (0.00)); //total
                    procedimiento.setInt(7, (numeroFactura)); //numeroFactura
                    procedimiento.setInt(8, (((Cliente)cmbCodigoCliente.getSelectionModel().getSelectedItem()).getCodigoCliente())); //codigoCliente
                    procedimiento.setInt(9, (((FormaPago)cmbCodigoFormaPago.getSelectionModel().getSelectedItem()).getCodigoFormaPago())); //codigoFormaPago
                    procedimiento.setInt(10, (((TipoProducto)cmbCodigoTipoProducto.getSelectionModel().getSelectedItem()).getCodigoTipoProducto())); //codigoTipoProducto
                    procedimiento.setInt(11, (((Usuario)cmbCodigoUsuario.getSelectionModel().getSelectedItem()).getCodigoUsuario())); //codigoUsuario
                    if(numeroFactura <= 0) {
                        JOptionPane.showMessageDialog(null, "El número de factura no puede ser 0 o negativo!");
                    }
                    if(numeroFactura > 0) {
                        procedimiento.execute();
                        camposCorrectos = true;
                        numeroFactura = 0;
                    }
                }while(numeroFactura > 0);
            }else {
                if(entrar) {
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    camposCorrectos = false;
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizar() {
        boolean entrar = true;
        int numeroFactura = 0;
        try{
            numeroFactura = Integer.parseInt((txtNumeroFactura.getText()));
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "El campo 'Número de Factura' debe de ser un número");
            entrar = false;
            camposCorrectos = false;
        }
        try{
            if(!(txtObservacion.getText().equals(""))&&!(txtNumeroFactura.getText().equals(""))&&!(cmbCodigoCliente.getValue().equals(""))&&!(cmbCodigoFormaPago.getValue().equals(""))&&!(cmbCodigoTipoProducto.getValue().equals(""))&&!(cmbCodigoUsuario.getValue().equals(""))&&(entrar)) {
                do{
                    PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_updateVenta(?,?,?,?,?,?,?,?,?,?,?)}");
                    procedimiento.setDate(1, (Date.valueOf(dtpFechaVenta.getValue()))); //fechaVenta
                    procedimiento.setInt(2, 12); //iva
                    procedimiento.setString(3, (txtObservacion.getText())); //observacion
                    procedimiento.setDate(4, (Date.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())))); //fechaModificacion
                    procedimiento.setInt(5, (((Cliente)cmbCodigoCliente.getSelectionModel().getSelectedItem()).getCodigoCliente())); //codigoCliente
                    procedimiento.setDouble(6, (0.00)); //total
                    procedimiento.setInt(7, (numeroFactura)); //numeroFactura
                    procedimiento.setInt(8, (((Cliente)cmbCodigoCliente.getSelectionModel().getSelectedItem()).getCodigoCliente())); //codigoCliente
                    procedimiento.setInt(9, (((FormaPago)cmbCodigoFormaPago.getSelectionModel().getSelectedItem()).getCodigoFormaPago())); //codigoFormaPago
                    procedimiento.setInt(10, (((TipoProducto)cmbCodigoTipoProducto.getSelectionModel().getSelectedItem()).getCodigoTipoProducto())); //codigoTipoProducto
                    procedimiento.setInt(11, (((Usuario)cmbCodigoUsuario.getSelectionModel().getSelectedItem()).getCodigoUsuario())); //codigoUsuario
                    procedimiento.setInt(12, (((Venta)tblVentas.getSelectionModel().getSelectedItem()).getCodigoVenta())); //codigoVenta
                    if(numeroFactura <= 0) {
                        JOptionPane.showMessageDialog(null, "El número de factura no puede ser 0 o negativo!");
                    }
                    if(numeroFactura > 0) {
                        procedimiento.execute();
                        camposCorrectos = true;
                        numeroFactura = 0;
                    }
                }while(numeroFactura > 0);
            }else {
                if(entrar) {
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    camposCorrectos = false;
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar() {
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_deleteVenta(?)}");
            procedimiento.setInt(1, (((Venta)tblVentas.getSelectionModel().getSelectedItem()).getCodigoVenta())); //codigoVenta
            procedimiento.execute();
        }catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Un registro depende de esta Venta");
        }
    }
    
    public void desactivarControles() {
        dtpFechaVenta.setDisable(true);
        txtObservacion.setDisable(true);
        cmbCodigoCliente.setDisable(true);
        cmbCodigoFormaPago.setDisable(true);
        cmbCodigoTipoProducto.setDisable(true);
        cmbCodigoUsuario.setDisable(true);
        txtNumeroFactura.setDisable(true);
        imgGuardarAgregar.setVisible(false);
        imgGuardarActualizar.setVisible(false);
        imgCancelar.setVisible(false);
    }
    
    public void activarControles() {
        dtpFechaVenta.setDisable(false);
        txtObservacion.setDisable(false);
        cmbCodigoCliente.setDisable(false);
        cmbCodigoFormaPago.setDisable(false);
        cmbCodigoTipoProducto.setDisable(false);
        cmbCodigoUsuario.setDisable(false);
        txtNumeroFactura.setDisable(false);
        imgGuardarAgregar.setVisible(true);
        imgGuardarActualizar.setVisible(true);
        imgCancelar.setVisible(true);
    }
    
    public void limpiarControles() {
        dtpFechaVenta.setValue(LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())));
        txtObservacion.setText("");
        cmbCodigoCliente.setValue("");
        cmbCodigoFormaPago.setValue("");
        cmbCodigoTipoProducto.setValue("");
        cmbCodigoUsuario.setValue("");
        txtNumeroFactura.setText("");
    }
    
    public void cancelar() {
        tblVentas.getSelectionModel().clearSelection();
        desactivarControles();
        limpiarControles();
        cargarDatos();
        imgAgregar.setVisible(true);
        imgActualizar.setVisible(true);
        imgEliminar.setVisible(true);
        btnAgregar.setVisible(true);
        btnActualizar.setVisible(true);
        operacionActual = operaciones.NINGUNO;
    }
    
    public void generarReporte(){
        if(tblVentas.getSelectionModel().getSelectedItem() != null){
            Map parametros = new HashMap();
            int codigoVenta = ((Venta)tblVentas.getSelectionModel().getSelectedItem()).getCodigoVenta();
            parametros.put("codigoVenta", codigoVenta);
            GenerarReporte.mostrarReporte("Venta.jasper", "Reporte de Factura", parametros);
        }else{
            JOptionPane.showMessageDialog(null,"Debe seleccionar una Venta");
        }
    }
    
    public void datePickerFormater(){
        dtpFechaVenta.setShowWeekNumbers(true);
        String pattern = "dd-MM-yyyy";
        dtpFechaVenta.setPromptText(pattern.toLowerCase());
        dtpFechaVenta.setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override 
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override 
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
    }
    
    public void detallesVenta() {
        if(tblVentas.getSelectionModel().getSelectedItem() != null) {
            this.escenario.detallesVenta(((Venta)tblVentas.getSelectionModel().getSelectedItem()).getCodigoVenta(), ((Venta)tblVentas.getSelectionModel().getSelectedItem()).getCodigoTipoProducto());
        }else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar una Venta");
        }
    }

    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }
    
    public void menu() {
        this.escenario.menu(this.escenario.getUsuario());
    }
    
    public void proveedores() {
        this.escenario.proveedores();
    }
    
    public void tiposProducto() {
        this.escenario.tiposProducto();
    }
    
    public void productos() {
        this.escenario.productos();
    }
    
    public void compras() {
        this.escenario.compras();
    }
    
    public void formasPago() {
        this.escenario.formasPago();
    }
    
    public void clientes() {
        this.escenario.clientes();
    }
    
    public void usuarios() {
        this.escenario.usuarios();
    }
    
    public void ventas() {
        this.escenario.ventas();
    }
    
    public void facturas() {
        this.escenario.facturas();
    }
    
}
