package org.julioherrera.controllers;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javax.swing.JOptionPane;
import org.julioherrera.beans.Proveedor;
import org.julioherrera.beans.TelefonoProveedor;
import org.julioherrera.db.Conexion;
import org.julioherrera.system.Main;

public class TelefonosProveedorController implements Initializable {
    
    private Main escenario;
    private enum operaciones {AGREGAR, ACTUALIZAR, NINGUNO}
    private operaciones operacionActual = operaciones.NINGUNO;
    private ObservableList<TelefonoProveedor> listaTelefonosProveedor;
    private ObservableList<Proveedor> listaProveedores;
    private boolean camposCorrectos;
    private int codigoProveedor;
    @FXML private TextField txtNumero;
    @FXML private TextField txtDescripcion;
    @FXML private TableView tblTelefonosProveedor;
    @FXML private TableColumn colCodigoTelefonoProveedor;
    @FXML private TableColumn colNumero;
    @FXML private TableColumn colDescripcion;
    @FXML private TableColumn colCodigoProveedor;
    @FXML private Circle btnAgregar;
    @FXML private Circle btnActualizar;
    @FXML private Circle btnEliminar;
    @FXML private ImageView imgAgregar;
    @FXML private ImageView imgActualizar;
    @FXML private ImageView imgEliminar;
    @FXML private ImageView imgGuardarAgregar;
    @FXML private ImageView imgGuardarActualizar;
    @FXML private ImageView imgCancelar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        desactivarControles();
    }
    
    public void cargarDatos() {
        tblTelefonosProveedor.setItems(getTelefonosProveedor());
        colCodigoTelefonoProveedor.setCellValueFactory(new PropertyValueFactory<TelefonoProveedor, Integer>("codigoTelefonoProveedor"));
        colNumero.setCellValueFactory(new PropertyValueFactory<TelefonoProveedor, String>("numero"));
        colDescripcion.setCellValueFactory(new PropertyValueFactory<TelefonoProveedor, String>("descripcion"));
        colCodigoProveedor.setCellValueFactory(new PropertyValueFactory<TelefonoProveedor, Integer>("codigoProveedor"));
    }
    
    public ObservableList<TelefonoProveedor> getTelefonosProveedor() {
        ArrayList<TelefonoProveedor> lista = new ArrayList<TelefonoProveedor>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_listTelefonosProveedor(?)}");
            procedimiento.setInt(1, codigoProveedor);
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                lista.add(new TelefonoProveedor(resultado.getInt("codigoTelefonoProveedor"), resultado.getString("numero"), resultado.getString("descripcion"), resultado.getInt("codigoProveedor")));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return listaTelefonosProveedor = FXCollections.observableArrayList(lista);
    }
    
    public void seleccionarElementos() {
        if(tblTelefonosProveedor.getSelectionModel().getSelectedIndex() > -1) {
            TelefonoProveedor elemento = (TelefonoProveedor)tblTelefonosProveedor.getSelectionModel().getSelectedItem();
            txtNumero.setText(elemento.getNumero());
            txtDescripcion.setText(elemento.getDescripcion());
        }
    }
    
    public void btnAgregar() {
        switch(operacionActual) {
            case NINGUNO:
                activarControles();
                limpiarControles();
                imgAgregar.setVisible(false);
                imgEliminar.setVisible(false);
                btnActualizar.setVisible(false);
                operacionActual = operaciones.AGREGAR;
            break;
            case AGREGAR:
                agregar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgAgregar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnActualizar.setVisible(true);
                    operacionActual = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnActualizar() {
        switch(operacionActual) {
            case NINGUNO:
                if(tblTelefonosProveedor.getSelectionModel().getSelectedItem() != null) {
                    activarControles();
                    imgActualizar.setVisible(false);
                    imgEliminar.setVisible(false);
                    btnAgregar.setVisible(false);
                    operacionActual = operaciones.ACTUALIZAR;
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Teléfono de Proveedor");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                if(camposCorrectos) {
                    desactivarControles();
                    cargarDatos();
                    imgActualizar.setVisible(true);
                    imgEliminar.setVisible(true);
                    btnAgregar.setVisible(true);
                    operacionActual = operaciones.NINGUNO;
                }
            break;
        }
    }
    
    public void btnEliminar() {
        switch(operacionActual) {
            case NINGUNO:
                if(tblTelefonosProveedor.getSelectionModel().getSelectedItem() != null) {
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar el registro?","Eliminar Teléfono de Proveedor",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION) {
                        eliminar();
                        limpiarControles();
                        cargarDatos();
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un Teléfono de Proveedor");
                }
            break;
            default:
                cancelar();
            break;
        }
    }
    
    public void agregar() {
        boolean entrar = true;
        int numero = 0;
        try{
            numero = Integer.parseInt((txtNumero.getText()));
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "El campo 'Número' debe de ser un número");
            entrar = false;
            camposCorrectos = false;
        }
        try{
            if(!(txtNumero.getText().equals(""))&&!(txtDescripcion.getText().equals(""))&&(entrar)) {
                do{
                    PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_insertTelefonoProveedor(?,?,?)}");
                    procedimiento.setString(1, (txtNumero.getText())); //numero
                    procedimiento.setString(2, (txtDescripcion.getText())); //descripcion
                    procedimiento.setInt(3, codigoProveedor); //codigoProveedor
                    if(numero <= 0) {
                        JOptionPane.showMessageDialog(null, "El número no puede ser 0 o negativo!");
                    }else {
                        procedimiento.execute();
                        camposCorrectos = true;
                        numero = 0;
                    }
                }while(numero > 0);
            }else {
                if(entrar) {
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    camposCorrectos = false;
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizar() {
        boolean entrar = true;
        int numero = 0;
        try{
            numero = Integer.parseInt((txtNumero.getText()));
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "El campo 'Número' debe de ser un número");
            entrar = false;
            camposCorrectos = false;
        }
        try{
            if(!(txtNumero.getText().equals(""))&&!(txtDescripcion.getText().equals(""))&&(entrar)) {
                do{
                    PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_updateTelefonoProveedor(?,?,?,?)}");
                    procedimiento.setString(1, (txtNumero.getText())); //numero
                    procedimiento.setString(2, (txtDescripcion.getText())); //descripcion
                    procedimiento.setInt(3, codigoProveedor); //codigoProveedor
                    procedimiento.setInt(4, (((TelefonoProveedor)tblTelefonosProveedor.getSelectionModel().getSelectedItem()).getCodigoTelefonoProveedor())); //codigoTelefonoProveedor
                    if(numero <= 0) {
                        JOptionPane.showMessageDialog(null, "El número no puede ser 0 o negativo!");
                    }else {
                        procedimiento.execute();
                        camposCorrectos = true;
                        numero = 0;
                    }
                }while(numero > 0);
            }else {
                if(entrar) {
                    JOptionPane.showMessageDialog(null, "¡Hay campos vacíos!");
                    camposCorrectos = false;
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar() {
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_deleteTelefonoProveedor(?)}");
            procedimiento.setInt(1, (((TelefonoProveedor)tblTelefonosProveedor.getSelectionModel().getSelectedItem()).getCodigoTelefonoProveedor())); //codigoTelefonoProveedor
            procedimiento.execute();
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void desactivarControles() {
        txtNumero.setDisable(true);
        txtDescripcion.setDisable(true);
        imgGuardarAgregar.setVisible(false);
        imgGuardarActualizar.setVisible(false);
        imgCancelar.setVisible(false);
    }
    
    public void activarControles() {
        txtNumero.setDisable(false);
        txtDescripcion.setDisable(false);
        imgGuardarAgregar.setVisible(true);
        imgGuardarActualizar.setVisible(true);
        imgCancelar.setVisible(true);
    }
    
    public void limpiarControles() {
        txtNumero.setText("");
        txtDescripcion.setText("");
    }
    
    public void cancelar() {
        tblTelefonosProveedor.getSelectionModel().clearSelection();
        desactivarControles();
        limpiarControles();
        cargarDatos();
        imgAgregar.setVisible(true);
        imgActualizar.setVisible(true);
        imgEliminar.setVisible(true);
        btnAgregar.setVisible(true);
        btnActualizar.setVisible(true);
        operacionActual = operaciones.NINGUNO;
    }
    
    public void proveedores() {
        this.escenario.proveedores();
    }

    public int getCodigoProveedor() {
        return codigoProveedor;
    }

    public void setCodigoProveedor(int codigoProveedor) {
        this.codigoProveedor = codigoProveedor;
        cargarDatos();
    }

    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }
    
}
