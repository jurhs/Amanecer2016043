package org.julioherrera.controllers;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javax.swing.JOptionPane;
import org.julioherrera.beans.Usuario;
import org.julioherrera.db.Conexion;
import org.julioherrera.system.Main;

public class LoginController implements Initializable {
    
    private Main escenario;
    @FXML private JFXTextField txtUsuario;
    @FXML private JFXPasswordField pssContrasena;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        txtUsuario.setText("");
        pssContrasena.setText("");
    }
    
    public Usuario getUsuario() {
        Usuario usuario = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call SP_searchUsuario(?)}");
            procedimiento.setString(1, (txtUsuario.getText()));
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()) {
                usuario = new Usuario(resultado.getInt("codigoUsuario"), resultado.getString("nombresUsuario"), resultado.getString("apellidosUsuario"), resultado.getString("usuario"), resultado.getString("contrasena"));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return usuario;
    }
    
    public void validacion() {
        String usuario;
        String contrasena;
        try{
            usuario = getUsuario().getUsuario();
            contrasena = getUsuario().getContrasena();
            if((usuario.equals(txtUsuario.getText())) && (contrasena.equals(pssContrasena.getText()))){
                menu();
            }else{
                JOptionPane.showMessageDialog(null, "¡Usuario y constraseña no coinciden!");
            }
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "¡El usuario no existe!");
        }
    }

    public void menu() {
        this.escenario.menu(getUsuario());
        this.escenario.setUsuario(getUsuario());
    }
    
    public Main getEscenario() {
        return escenario;
    }

    public void setEscenario(Main escenario) {
        this.escenario = escenario;
    }
    
}
