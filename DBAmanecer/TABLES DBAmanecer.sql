USE master
CREATE DATABASE DBAmanecer2016043
GO
USE DBAmanecer2016043
GO

--CREACI�N DE TABLAS--

CREATE TABLE Proveedores (
	codigoProveedor INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	razonSocial VARCHAR(128) NOT NULL,
	nit VARCHAR(32) NOT NULL,
	direccionProveedor VARCHAR(128) NOT NULL,
	paginaWeb VARCHAR(64)
)

CREATE TABLE TelefonosProveedor(
	codigoTelefonoProveedor INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	numero VARCHAR(16) NOT NULL,
	descripcion VARCHAR(32),
	codigoProveedor INT NOT NULL,
	CONSTRAINT FK_TelefonoProveedor_Proveedores FOREIGN KEY (codigoProveedor) REFERENCES Proveedores (codigoProveedor)
)

CREATE TABLE TiposProducto(
	codigoTipoProducto INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	descripcion VARCHAR(128),
	fechaCreacion DATE NOT NULL,
	fechaModificacion DATE NOT NULL
)

CREATE TABLE Productos(
	codigoProducto INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	nombreProducto VARCHAR(128) NOT NULL,
	existencia INT NOT NULL DEFAULT 0,
	precioCosto DECIMAL(10,2) NOT NULL DEFAULT 0,
	precioVenta DECIMAL(10,2) NOT NULL DEFAULT 0,
	productoIva DECIMAL(10,2) NOT NULL,
	fechaCreacion DATE NOT NULL,
	fechaModificacion DATE NOT NULL,
	codigoTipoProducto INT NOT NULL,
	codigoProveedor INT NOT NULL,
	CONSTRAINT FK_Productos_TipoProductos FOREIGN KEY (codigoTipoProducto) REFERENCES TiposProducto (codigoTipoProducto),
	CONSTRAINT FK_Productos_Proveedores FOREIGN KEY (codigoProveedor) REFERENCES Proveedores (codigoProveedor)
)

CREATE TABLE Compras(
	codigoCompra INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	descripcion VARCHAR(128) NOT NULL,
	precioUnitario DECIMAL(10,2) NOT NULL,
	cantidad INT NOT NULL,
	total DECIMAL(10,2),
	codigoProveedor INT NOT NULL,
	codigoProducto INT NOT NULL,
	CONSTRAINT FK_Compras_Proveedores FOREIGN KEY (codigoProveedor) REFERENCES Proveedores (codigoProveedor),
	CONSTRAINT FK_Compras_Productos FOREIGN KEY (codigoProducto) REFERENCES Productos (codigoProducto)
)

CREATE TABLE FormasPago(
	codigoFormaPago INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	descripcion VARCHAR(128) NOT NULL
)

CREATE TABLE Clientes(
	codigoCliente INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	nombresCliente VARCHAR(32) NOT NULL,
	apellidosCliente VARCHAR(32) NOT NULL,
	direccionCliente VARCHAR(128),
	nitCliente VARCHAR(32) NOT NULL,
	fechaCreacion DATE NOT NULL,
	fechaModificacion DATE NOT NULL
)

CREATE TABLE EmailsCliente(
	codigoEmailCliente INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	email VARCHAR(32) NOT NULL,
	descripcion VARCHAR(64),
	codigoCliente INT NOT NULL,
	CONSTRAINT FK_EmailCliente_Clientes FOREIGN KEY (codigoCliente) REFERENCES Clientes (codigoCliente)
)

CREATE TABLE TelefonosCliente(
	codigoTelefonoCliente INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	telefono VARCHAR(16) NOT NULL,
	descripcion VARCHAR(64),
	codigoCliente INT NOT NULL,
	CONSTRAINT FK_TelefonoCliente_Clientes FOREIGN KEY (codigoCliente) REFERENCES Clientes (codigoCliente)
)

CREATE TABLE Usuarios(
	codigoUsuario INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	nombresUsuario VARCHAR(32) NOT NULL UNIQUE,
	apellidosUsuario VARCHAR(32) NOT NULL,
	usuario VARCHAR(32) NOT NULL,
	contrasena VARCHAR(32) NOT NULL 
)

CREATE TABLE Ventas(
	codigoVenta INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	fechaVenta DATE NOT NULL,
	iva INT NOT NULL DEFAULT 12,
	observacion VARCHAR(256),
	fechaCreacion DATE NOT NULL,
	fechaModificacion DATE NOT NULL,
	total DECIMAL(10,2) NOT NULL DEFAULT 0,
	numeroFactura INT NOT NULL UNIQUE,
	codigoCliente INT NOT NULL,
	codigoFormaPago INT NOT NULL,
	codigoTipoProducto INT NOT NULL,
	codigoUsuario INT NOT NULL,
	CONSTRAINT FK_Ventas_Clientes FOREIGN KEY (codigoCliente) REFERENCES Clientes (codigoCliente),
	CONSTRAINT FK_Ventas_FormaPago FOREIGN KEY (codigoFormaPago) REFERENCES FormasPago (codigoFormaPago),
	CONSTRAINT FK_Ventas_TipoProducto FOREIGN KEY (codigoTipoProducto) REFERENCES TiposProducto (codigoTipoProducto),
	CONSTRAINT FK_Ventas_Usuarios FOREIGN KEY (codigoUsuario) REFERENCES Usuarios (codigoUsuario)  
)

CREATE TABLE DetallesVenta(
	codigoDetalleVenta INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	cantidad INT NOT NULL,
	precioVenta DECIMAL(10,2) NOT NULL,
	productoIva DECIMAL(10,2) NOT NULL,
	codigoVenta INT NOT NULL,
	codigoProducto INT NOT NULL,
	CONSTRAINT FK_DetalleVentas_Ventas FOREIGN KEY (codigoVenta) REFERENCES Ventas (codigoVenta),
	CONSTRAINT FK_DetalleVentas_Productos FOREIGN KEY (codigoProducto) REFERENCES Productos (codigoProducto)
)

CREATE TABLE Facturas(
	codigoFactura INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	numeroFactura VARCHAR(8) NOT NULL,
	fecha DATE,
	nitCliente VARCHAR(32),
	cantidad INT,
	precioVenta DECIMAL(10,2),
	total DECIMAL(10,2),
	codigoProducto INT,
	codigoCliente INT,
	CONSTRAINT FK_Factura_Productos FOREIGN KEY (codigoProducto) REFERENCES Productos (codigoProducto),
	CONSTRAINT FK_Factura_Clientes FOREIGN KEY (codigoCliente) REFERENCES Clientes (codigoCliente)
)