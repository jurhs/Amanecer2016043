USE DBAmanecer2016043
GO

--CREACI�N DE PROCEDIMIENTOS ALMACENADOS--

--PROVEEDORES---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_insertProveedor (@razonSocial VARCHAR(128), @nit VARCHAR(32), @direccionProveedor VARCHAR(128),
									@paginaWeb VARCHAR(65))
AS
BEGIN
	INSERT INTO Proveedores VALUES (@razonSocial, @nit, @direccionProveedor, @paginaWeb)
END
GO
--UPDATE--
CREATE PROCEDURE SP_updateProveedor (@razonSocial VARCHAR(128), @nit VARCHAR(32), @direccionProveedor VARCHAR(128),
									@paginaWeb VARCHAR(65), @codigoProveedor INT)
AS
BEGIN
	UPDATE Proveedores SET razonSocial = @razonSocial, nit = @nit, direccionProveedor = @direccionProveedor,
							paginaWeb = @paginaWeb WHERE codigoProveedor = @codigoProveedor
END
GO
--DELETE--
CREATE PROCEDURE SP_deleteProveedor (@codigoProveedor INT)
AS
BEGIN
	DELETE Proveedores WHERE codigoProveedor = @codigoProveedor
END
GO
--LIST--
CREATE PROCEDURE SP_listProveedores
AS
BEGIN
	SELECT * FROM Proveedores
END
GO

--TELEFONOS PROVEEDOR-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_insertTelefonoProveedor (@numero VARCHAR(16), @descripcion VARCHAR(32), @codigoProveedor INT)
AS
BEGIN
	INSERT INTO TelefonosProveedor VALUES (@numero, @descripcion, @codigoProveedor)
END
GO
--UPDATE--
CREATE PROCEDURE SP_updateTelefonoProveedor (@numero VARCHAR(16), @descripcion VARCHAR(32),
												@codigoProveedor INT, @codigoTelefonoProveedor INT)
AS
BEGIN
	UPDATE TelefonosProveedor SET numero = @numero, descripcion = @descripcion, codigoProveedor = @codigoProveedor
								WHERE codigoTelefonoProveedor = @codigoTelefonoProveedor
END
GO
--DELETE--
CREATE PROCEDURE SP_deleteTelefonoProveedor (@codigoTelefonoProveedor INT)
AS
BEGIN
	DELETE TelefonosProveedor WHERE codigoTelefonoProveedor = @codigoTelefonoProveedor
END
GO
--LIST--
CREATE PROCEDURE SP_listTelefonosProveedor (@codigoProveedor INT)
AS
BEGIN
	SELECT * FROM TelefonosProveedor WHERE codigoProveedor = @codigoProveedor
END
GO

--TIPOS PRODUCTO-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_insertTipoProducto (@descripcion VARCHAR(128), @fechaCreacion DATE, @fechaModificacion DATE)
AS
BEGIN
	INSERT INTO TiposProducto VALUES (@descripcion, @fechaCreacion, @fechaModificacion)
END
GO
--UPDATE--
CREATE PROCEDURE SP_updateTipoProducto (@descripcion VARCHAR(128), @fechaModificacion DATE,
										@codigoTipoProducto INT)
AS
BEGIN
	UPDATE TiposProducto SET descripcion = @descripcion, fechaModificacion = @fechaModificacion
						WHERE codigoTipoProducto = @codigoTipoProducto
END
GO
--DELETE--
CREATE PROCEDURE SP_deleteTipoProducto (@codigoTipoProducto INT)
AS
BEGIN
	DELETE TiposProducto WHERE codigoTipoProducto = @codigoTipoProducto
END
GO
--LIST--
CREATE PROCEDURE SP_listTiposProducto
AS
BEGIN
	SELECT * FROM TiposProducto
END
GO

--PRODUCTOS-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_insertProducto (@nombreProducto VARCHAR(128), @existencia INT, @precioCosto DECIMAL(10,2), 
									@precioVenta DECIMAL(10,2), @productoIva DECIMAL(10,2), @fechaCreacion DATE, 
									@fechaModificacion DATE, @codigoTipoProducto INT, @codigoProveedor INT)
AS
BEGIN
	INSERT INTO Productos VALUES (@nombreProducto, @existencia, @precioCosto, @precioVenta, @productoIva, @fechaCreacion,
									@fechaModificacion, @codigoTipoProducto, @codigoProveedor)
END
GO
--UPDATE--
CREATE PROCEDURE SP_updateProducto (@nombreProducto VARCHAR(128), @fechaModificacion DATE, @codigoTipoProducto INT,
									@codigoProveedor INT, @codigoProducto INT)
AS																								
BEGIN
	UPDATE Productos SET nombreProducto = @nombreProducto, fechaModificacion = @fechaModificacion,
							codigoTipoProducto = @codigoTipoProducto, codigoProveedor = @codigoProveedor
							WHERE codigoProducto = @codigoProducto
END
GO
--DELETE--
CREATE PROCEDURE SP_deleteProducto (@codigoProducto INT)
AS
BEGIN
	DELETE Productos WHERE codigoProducto = @codigoProducto
END
GO
--LIST--
CREATE PROCEDURE SP_listProductos
AS
BEGIN
	SELECT * FROM Productos P JOIN TiposProducto T On P.codigoTipoProducto = T.codigoTipoProducto JOIN Proveedores O On P.codigoProveedor = O.codigoProveedor
END
GO

--COMPRAS-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_insertCompra (@descripcion VARCHAR(128), @precioUnitario DECIMAL(10,2), @cantidad INT,
									@total DECIMAL(10,2), @codigoProveedor INT, @codigoProducto INT)
AS
BEGIN
	INSERT INTO Compras VALUES(@descripcion, @precioUnitario, @cantidad, @total, @codigoProveedor, @codigoProducto)
END
GO
--UPDATE--
CREATE PROCEDURE SP_updateCompra (@descripcion VARCHAR(128), @precioUnitario DECIMAL(10,2), @cantidad INT,
									@total DECIMAL(10,2), @codigoProveedor INT, @codigoProducto INT, @codigoCompra INT)
AS
BEGIN
	UPDATE Compras SET descripcion = @descripcion, precioUnitario = @precioUnitario, cantidad = @cantidad, total = @total,
						codigoProveedor = @codigoProveedor, codigoProducto = @codigoProducto WHERE codigoCompra = @codigoCompra
END
GO
--DELETE--
CREATE PROCEDURE SP_deleteCompra (@codigoCompra INT)
AS
BEGIN
	DELETE Compras WHERE codigoCompra = @codigoCompra
END
GO
--LIST--
CREATE PROCEDURE SP_listCompras
AS
BEGIN
	SELECT * FROM Compras
END
GO

--Formas Pago-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_insertFormaPago (@descripcion VARCHAR(128))
AS
BEGIN
	INSERT INTO FormasPago VALUES (@descripcion)
END
GO
--UPDATE--
CREATE PROCEDURE SP_updateFormaPago (@descripcion VARCHAR(128), @codigoFormaPago INT)
AS
BEGIN
	UPDATE FormasPago SET descripcion = @descripcion WHERE codigoFormaPago = @codigoFormaPago
END
GO
--DELETE--
CREATE PROCEDURE SP_deleteFormaPago (@codigoFormaPago INT)
AS
BEGIN
	DELETE FormasPago WHERE codigoFormaPago = @codigoFormaPago
END
GO
--LIST--
CREATE PROCEDURE SP_listFormasPago
AS
BEGIN
	SELECT * FROM FormasPago
END
GO

--CLIENTES-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_insertCliente (@nombresCliente VARCHAR(32), @apellidosCliente VARCHAR(32), @direccionCliente VARCHAR(128),
									@nitCliente VARCHAR(32), @fechaCreacion DATE, @fechaModificacion DATE)
AS
BEGIN
	INSERT INTO Clientes VALUES (@nombresCliente, @apellidosCliente, @direccionCliente, @nitCliente, @fechaCreacion, @fechaModificacion)
END
GO
--UPDATE--
CREATE PROCEDURE SP_updateCliente (@nombresCliente VARCHAR(32), @apellidosCliente VARCHAR(32), @direccionCliente VARCHAR(128),
									@nitCliente VARCHAR(32), @fechaModificacion DATE, @codigoCliente INT)
AS
BEGIN
	UPDATE Clientes SET nombresCliente = @nombresCliente, apellidosCliente = @apellidosCliente,
						direccionCliente = @direccionCliente, nitCliente = @nitCliente,
						fechaModificacion = @fechaModificacion WHERE codigoCliente = @codigoCliente
END
GO
--DELETE--
CREATE PROCEDURE SP_deleteCliente (@codigoCliente INT)
AS
BEGIN
	DELETE Clientes WHERE codigoCliente = @codigoCliente
END
GO
--LIST--
CREATE PROCEDURE SP_listClientes
AS
BEGIN
	SELECT * FROM Clientes
END
GO

--EMAILS CLIENTE-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_insertEmailCliente (@email VARCHAR(32), @descripcion VARCHAR(64), @codigoCliente INT)
AS
BEGIN
	INSERT INTO EmailsCliente VALUES (@email, @descripcion, @codigoCliente)
END
GO
--UPDATE--
CREATE PROCEDURE SP_updateEmailCliente (@email VARCHAR(32), @descripcion VARCHAR(64), @codigoCliente INT, @codigoEmailCliente INT)
AS
BEGIN
	UPDATE EmailsCliente SET email = @email, descripcion = @descripcion, codigoCliente = @codigoCliente
							WHERE codigoEmailCliente = @codigoEmailCliente
END
GO
--DELETE--
CREATE PROCEDURE SP_deleteEmailCliente (@codigoEmailCliente INT)
AS
BEGIN
	DELETE EmailsCliente WHERE codigoEmailCliente = @codigoEmailCliente
END
GO
--LIST--
CREATE PROCEDURE SP_listEmailsCliente (@codigoCliente INT)
AS
BEGIN
	SELECT * FROM EmailsCliente WHERE codigoCliente = @codigoCliente
END
GO

--TELEFONOS CLIENTE-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_insertTelefonoCliente (@telefono VARCHAR(16), @descripcion VARCHAR(64), @codigoCliente INT)
AS
BEGIN
	INSERT INTO TelefonosCliente VALUES (@telefono, @descripcion, @codigoCliente)
END
GO
--UPDATE--
CREATE PROCEDURE SP_updateTelefonoCliente (@telefono VARCHAR(16), @descripcion VARCHAR(64), @codigoCliente INT, @codigoTelefonoCliente INT)
AS
BEGIN
	UPDATE TelefonosCliente SET telefono = @telefono, descripcion = @descripcion, codigoCliente = @codigoCliente
							WHERE codigoTelefonoCliente = @codigoTelefonoCliente
END
GO
--DELETE--
CREATE PROCEDURE SP_deleteTelefonoCliente (@codigoTelefonoCliente INT)
AS
BEGIN
	DELETE TelefonosCliente WHERE codigoTelefonoCliente = @codigoTelefonoCliente
END
GO
--LIST--
CREATE PROCEDURE SP_listTelefonosCliente (@codigoCliente INT)
AS
BEGIN
	SELECT * FROM TelefonosCliente WHERE codigoCliente = @codigoCliente
END
GO

--USUARIOS-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_insertUsuario (@nombresUsuario VARCHAR(32), @apellidosUsuario VARCHAR(32), @usuario VARCHAR(32), @contrasena VARCHAR(32))
AS
BEGIN
	INSERT INTO Usuarios VALUES (@nombresUsuario, @apellidosUsuario, @usuario, @contrasena)
END
GO
--UPDATE--
CREATE PROCEDURE SP_updateUsuario (@nombresUsuario VARCHAR(32), @apellidosUsuario VARCHAR(32), @usuario VARCHAR(32), @contrasena VARCHAR(32),
									@codigoUsuario INT)
AS
BEGIN
	UPDATE Usuarios SET nombresUsuario = @nombresUsuario, apellidosUsuario = @apellidosUsuario, usuario = @usuario,
						contrasena = @contrasena WHERE codigoUsuario = @codigoUsuario
END
GO
--DELETE--
CREATE PROCEDURE SP_deleteUsuario (@codigoUsuario INT)
AS
BEGIN
	DELETE Usuarios WHERE codigoUsuario = @codigoUsuario
END
GO
--LIST--
CREATE PROCEDURE SP_listUsuarios
AS
BEGIN
	SELECT * FROM Usuarios
END
GO
--SEARCH--
CREATE PROCEDURE SP_searchUsuario (@usuario VARCHAR(32))
AS
BEGIN
	SELECT * FROM Usuarios WHERE usuario = @usuario
END
GO

--VENTAS-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_insertVenta (@fechaVenta DATE, @iva INT, @observacion VARCHAR(256), @fechaCreacion DATE, @fechaModificacion DATE,
								 @total DECIMAL(10,2), @numeroFactura INT, @codigoCliente INT, @codigoFormaPago INT, @codigoTipoProducto INT,
								 @codigoUsuario INT)
AS
BEGIN
	INSERT INTO Ventas VALUES (@fechaVenta, @iva, @observacion, @fechaCreacion, @fechaModificacion, @total, @numeroFactura, @codigoCliente,
								@codigoFormaPago, @codigoTipoProducto, @codigoUsuario)
END
GO
--UPDATE--
CREATE PROCEDURE SP_updateVenta (@fechaVenta DATE, @iva INT, @observacion VARCHAR(256), @fechaModificacion DATE, @total DECIMAL(10,2),
								@numeroFactura INT, @codigoCliente INT, @codigoFormaPago INT, @codigoTipoProducto INT, @codigoUsuario INT,
								@codigoVenta INT)
AS
BEGIN
	UPDATE Ventas SET fechaVenta = @fechaVenta, iva = @iva, observacion = @observacion, fechaModificacion = @fechaModificacion,
						total = @total, numeroFactura = @numeroFactura, codigoCliente = @codigoCliente, codigoFormaPago = @codigoFormaPago,
						codigoTipoProducto = @codigoTipoProducto, codigoUsuario = @codigoUsuario WHERE codigoVenta = @codigoVenta
END
GO
--DELETE--
CREATE PROCEDURE SP_deleteVenta (@codigoVenta INT)
AS
BEGIN
	DELETE Ventas WHERE codigoVenta = @codigoVenta
END
GO
--LIST--
CREATE PROCEDURE SP_listVentas
AS
BEGIN
	SELECT * FROM Ventas
END
GO
--SEARCH--
CREATE PROCEDURE SP_searchVenta (@codigoVenta INT)
AS
BEGIN
	SELECT V.codigoVenta, V.fechaVenta, V.iva, V.observacion, V.fechaCreacion, V.fechaModificacion, V.total, V.numeroFactura, C.nombresCliente,
	C.apellidosCliente, C.nitCliente, F.descripcion AS 'formaPago', T.descripcion AS 'tipoProducto', U.nombresUsuario, U.apellidosUsuario
	FROM Ventas V JOIN Clientes C ON V.codigoCliente = C.codigoCliente JOIN FormasPago F ON V.codigoFormaPago = F.codigoFormaPago
	JOIN TiposProducto T ON T.codigoTipoProducto = V.codigoTipoProducto JOIN Usuarios U ON V.codigoUsuario = U.codigoUsuario
	WHERE V.codigoVenta = @codigoVenta
END
GO

--DETALLES VENTA-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_insertDetalleVenta (@cantidad INT, @precioVenta DECIMAL(10,2), @productoIva DECIMAL(10,2), @codigoVenta INT, @codigoProducto INT,
										@numeroFactura INT, @fecha DATE, @nitCliente VARCHAR(32), @total DECIMAL(10,2), @codigoCliente INT)
AS
BEGIN
	INSERT INTO DetallesVenta VALUES (@cantidad, @precioVenta, @productoIva, @codigoVenta, @codigoProducto)
	INSERT INTO Facturas VALUES (@numeroFactura, @fecha, @nitCliente, @cantidad, @precioVenta, @total, @codigoProducto, @codigoCliente)
END
GO
--UPDATE--
CREATE PROCEDURE SP_updateDetalleVenta (@cantidad INT, @precioVenta DECIMAL(10,2), @productoIva DECIMAL(10,2), @codigoVenta INT,
										@codigoProducto INT, @codigoDetalleVenta INT, @numeroFactura INT, @fecha DATE, @nitCliente VARCHAR(32),
										@total DECIMAL(10,2), @codigoCliente INT, @codigoFactura INT)
AS
BEGIN
	UPDATE DetallesVenta SET cantidad = @cantidad, precioVenta = @precioVenta, productoIva = @productoIva, codigoVenta = @codigoVenta,
								codigoProducto = @codigoProducto WHERE codigoDetalleVenta = @codigoDetalleVenta
	UPDATE Facturas SET numeroFactura = @numeroFactura, fecha = @fecha, nitCliente = @nitCliente, cantidad = @cantidad, precioVenta = @precioVenta,
						total = @total, codigoProducto = @codigoProducto, codigoCliente = @codigoCliente WHERE codigoFactura = @codigoFactura
END
GO
--DELETE--
CREATE PROCEDURE SP_deleteDetalleVenta (@codigoDetalleVenta INT, @codigoFactura INT)
AS
BEGIN
	DELETE DetallesVenta WHERE codigoDetalleVenta = @codigoDetalleVenta
	DELETE Facturas WHERE codigoFactura = @codigoFactura
END
GO
--LIST--
CREATE PROCEDURE SP_listDetallesVenta (@codigoVenta INT)
AS
BEGIN
	SELECT * FROM DetallesVenta WHERE codigoVenta = @codigoVenta
END
GO

--FACTURAS-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--INSERT--
CREATE PROCEDURE SP_insertFactura (@numeroFactura VARCHAR(8), @fecha DATE, @nitCliente VARCHAR(32), @cantidad INT, @precioVenta DECIMAL(10,2),
									@total DECIMAL(10,2), @codigoProducto INT, @codigoCliente INT)
AS
BEGIN
	INSERT INTO Facturas VALUES (@numeroFactura, @fecha, @nitCliente, @cantidad, @precioVenta, @total, @codigoProducto, @codigoCliente)
END
GO
--UPDATE--
CREATE PROCEDURE SP_updateFactura (@numeroFactura VARCHAR(8), @fecha DATE, @nitCliente VARCHAR(32), @cantidad INT, @precioVenta DECIMAL(10,2),
									@total DECIMAL(10,2), @codigoProducto INT, @codigoCliente INT, @codigoFactura INT)
AS
BEGIN
	UPDATE Facturas SET numeroFactura = @numeroFactura, fecha = @fecha, nitCliente = @nitCliente, cantidad = @cantidad, precioVenta = @precioVenta,
						total = @total, codigoProducto = @codigoProducto, codigoCliente = @codigoCliente WHERE codigoFactura = @codigoFactura
END
GO
--DELETE--
CREATE PROCEDURE SP_deleteFactura (@codigoFactura INT)
AS
BEGIN
	DELETE Facturas WHERE codigoFactura = @codigoFactura
END
GO
--LIST--
CREATE PROCEDURE SP_listFacturas
AS
BEGIN
	SELECT * FROM Facturas
END
GO
--SEARCH--
CREATE PROCEDURE SP_searchFactura (@numeroFactura INT)
AS
BEGIN
	SELECT * FROM Facturas F JOIN Productos P ON F.codigoProducto = P.codigoProducto WHERE numeroFactura = @numeroFactura
END
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--APP DEFAULT DATA--

--USUARIO POR DEFECTO--
execute SP_insertUsuario 'admin', 'admin', 'admin', 'admin'

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
